<?php

namespace Cashu\TPay\Catalogs\Arrays;

use \WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfsalesChannel Arrays
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfsalesChannel
 * @subpackage Arrays
 */
class ArrayOfsalesChannel extends AbstractStructArrayBase
{
    /**
     * The salesChannel
     * Meta informations extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \Cashu\TPay\Catalogs\Structs\SalesChannel[]
     */
    public $salesChannel;
    /**
     * Constructor method for ArrayOfsalesChannel
     * @uses ArrayOfsalesChannel::setSalesChannel()
     * @param \Cashu\TPay\Catalogs\Structs\SalesChannel[] $salesChannel
     */
    public function __construct(array $salesChannel = array())
    {
        $this
            ->setSalesChannel($salesChannel);
    }
    /**
     * Get salesChannel value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \Cashu\TPay\Catalogs\Structs\SalesChannel[]|null
     */
    public function getSalesChannel()
    {
        return isset($this->salesChannel) ? $this->salesChannel : null;
    }
    /**
     * Set salesChannel value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \Cashu\TPay\Catalogs\Structs\SalesChannel[] $salesChannel
     * @return \Cashu\TPay\Catalogs\Arrays\ArrayOfsalesChannel
     */
    public function setSalesChannel(array $salesChannel = array())
    {
        foreach ($salesChannel as $arrayOfsalesChannelSalesChannelItem) {
            // validation for constraint: itemType
            if (!$arrayOfsalesChannelSalesChannelItem instanceof \Cashu\TPay\Catalogs\Structs\SalesChannel) {
                throw new \InvalidArgumentException(sprintf('The salesChannel property can only contain items of \Cashu\TPay\Catalogs\Structs\SalesChannel, "%s" given', is_object($arrayOfsalesChannelSalesChannelItem) ? get_class($arrayOfsalesChannelSalesChannelItem) : gettype($arrayOfsalesChannelSalesChannelItem)), __LINE__);
            }
        }
        if (is_null($salesChannel) || (is_array($salesChannel) && empty($salesChannel))) {
            unset($this->salesChannel);
        } else {
            $this->salesChannel = $salesChannel;
        }
        return $this;
    }
    /**
     * Add item to salesChannel value
     * @throws \InvalidArgumentException
     * @param \Cashu\TPay\Catalogs\Structs\SalesChannel $item
     * @return \Cashu\TPay\Catalogs\Arrays\ArrayOfsalesChannel
     */
    public function addToSalesChannel(\Cashu\TPay\Catalogs\Structs\SalesChannel $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \Cashu\TPay\Catalogs\Structs\SalesChannel) {
            throw new \InvalidArgumentException(sprintf('The salesChannel property can only contain items of \Cashu\TPay\Catalogs\Structs\SalesChannel, "%s" given', is_object($item) ? get_class($item) : gettype($item)), __LINE__);
        }
        $this->salesChannel[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \Cashu\TPay\Catalogs\Structs\SalesChannel|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \Cashu\TPay\Catalogs\Structs\SalesChannel|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \Cashu\TPay\Catalogs\Structs\SalesChannel|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \Cashu\TPay\Catalogs\Structs\SalesChannel|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \Cashu\TPay\Catalogs\Structs\SalesChannel|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string salesChannel
     */
    public function getAttributeName()
    {
        return 'salesChannel';
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructArrayBase::__set_state()
     * @uses AbstractStructArrayBase::__set_state()
     * @param array $array the exported values
     * @return \Cashu\TPay\Catalogs\Arrays\ArrayOfsalesChannel
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}

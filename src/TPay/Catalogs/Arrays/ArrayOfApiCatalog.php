<?php

namespace Cashu\TPay\Catalogs\Arrays;

use \WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfApiCatalog Arrays
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfApiCatalog
 * @subpackage Arrays
 */
class ArrayOfApiCatalog extends AbstractStructArrayBase
{
    /**
     * The ApiCatalog
     * Meta informations extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \Cashu\TPay\Catalogs\Structs\ApiCatalog[]
     */
    public $ApiCatalog;
    /**
     * Constructor method for ArrayOfApiCatalog
     * @uses ArrayOfApiCatalog::setApiCatalog()
     * @param \Cashu\TPay\Catalogs\Structs\ApiCatalog[] $apiCatalog
     */
    public function __construct(array $apiCatalog = array())
    {
        $this
            ->setApiCatalog($apiCatalog);
    }
    /**
     * Get ApiCatalog value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \Cashu\TPay\Catalogs\Structs\ApiCatalog[]|null
     */
    public function getApiCatalog()
    {
        return isset($this->ApiCatalog) ? $this->ApiCatalog : null;
    }
    /**
     * Set ApiCatalog value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \Cashu\TPay\Catalogs\Structs\ApiCatalog[] $apiCatalog
     * @return \Cashu\TPay\Catalogs\Arrays\ArrayOfApiCatalog
     */
    public function setApiCatalog(array $apiCatalog = array())
    {
        foreach ($apiCatalog as $arrayOfApiCatalogApiCatalogItem) {
            // validation for constraint: itemType
            if (!$arrayOfApiCatalogApiCatalogItem instanceof \Cashu\TPay\Catalogs\Structs\ApiCatalog) {
                throw new \InvalidArgumentException(sprintf('The ApiCatalog property can only contain items of \Cashu\TPay\Catalogs\Structs\ApiCatalog, "%s" given', is_object($arrayOfApiCatalogApiCatalogItem) ? get_class($arrayOfApiCatalogApiCatalogItem) : gettype($arrayOfApiCatalogApiCatalogItem)), __LINE__);
            }
        }
        if (is_null($apiCatalog) || (is_array($apiCatalog) && empty($apiCatalog))) {
            unset($this->ApiCatalog);
        } else {
            $this->ApiCatalog = $apiCatalog;
        }
        return $this;
    }
    /**
     * Add item to ApiCatalog value
     * @throws \InvalidArgumentException
     * @param \Cashu\TPay\Catalogs\Structs\ApiCatalog $item
     * @return \Cashu\TPay\Catalogs\Arrays\ArrayOfApiCatalog
     */
    public function addToApiCatalog(\Cashu\TPay\Catalogs\Structs\ApiCatalog $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \Cashu\TPay\Catalogs\Structs\ApiCatalog) {
            throw new \InvalidArgumentException(sprintf('The ApiCatalog property can only contain items of \Cashu\TPay\Catalogs\Structs\ApiCatalog, "%s" given', is_object($item) ? get_class($item) : gettype($item)), __LINE__);
        }
        $this->ApiCatalog[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \Cashu\TPay\Catalogs\Structs\ApiCatalog|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \Cashu\TPay\Catalogs\Structs\ApiCatalog|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \Cashu\TPay\Catalogs\Structs\ApiCatalog|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \Cashu\TPay\Catalogs\Structs\ApiCatalog|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \Cashu\TPay\Catalogs\Structs\ApiCatalog|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string ApiCatalog
     */
    public function getAttributeName()
    {
        return 'ApiCatalog';
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructArrayBase::__set_state()
     * @uses AbstractStructArrayBase::__set_state()
     * @param array $array the exported values
     * @return \Cashu\TPay\Catalogs\Arrays\ArrayOfApiCatalog
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}

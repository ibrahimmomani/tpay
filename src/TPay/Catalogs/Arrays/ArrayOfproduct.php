<?php

namespace Cashu\TPay\Catalogs\Arrays;

use \WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfproduct Arrays
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfproduct
 * @subpackage Arrays
 */
class ArrayOfproduct extends AbstractStructArrayBase
{
    /**
     * The product
     * Meta informations extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \Cashu\TPay\Catalogs\Structs\Product[]
     */
    public $product;
    /**
     * Constructor method for ArrayOfproduct
     * @uses ArrayOfproduct::setProduct()
     * @param \Cashu\TPay\Catalogs\Structs\Product[] $product
     */
    public function __construct(array $product = array())
    {
        $this
            ->setProduct($product);
    }
    /**
     * Get product value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \Cashu\TPay\Catalogs\Structs\Product[]|null
     */
    public function getProduct()
    {
        return isset($this->product) ? $this->product : null;
    }
    /**
     * Set product value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \Cashu\TPay\Catalogs\Structs\Product[] $product
     * @return \Cashu\TPay\Catalogs\Arrays\ArrayOfproduct
     */
    public function setProduct(array $product = array())
    {
        foreach ($product as $arrayOfproductProductItem) {
            // validation for constraint: itemType
            if (!$arrayOfproductProductItem instanceof \Cashu\TPay\Catalogs\Structs\Product) {
                throw new \InvalidArgumentException(sprintf('The product property can only contain items of \Cashu\TPay\Catalogs\Structs\Product, "%s" given', is_object($arrayOfproductProductItem) ? get_class($arrayOfproductProductItem) : gettype($arrayOfproductProductItem)), __LINE__);
            }
        }
        if (is_null($product) || (is_array($product) && empty($product))) {
            unset($this->product);
        } else {
            $this->product = $product;
        }
        return $this;
    }
    /**
     * Add item to product value
     * @throws \InvalidArgumentException
     * @param \Cashu\TPay\Catalogs\Structs\Product $item
     * @return \Cashu\TPay\Catalogs\Arrays\ArrayOfproduct
     */
    public function addToProduct(\Cashu\TPay\Catalogs\Structs\Product $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \Cashu\TPay\Catalogs\Structs\Product) {
            throw new \InvalidArgumentException(sprintf('The product property can only contain items of \Cashu\TPay\Catalogs\Structs\Product, "%s" given', is_object($item) ? get_class($item) : gettype($item)), __LINE__);
        }
        $this->product[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \Cashu\TPay\Catalogs\Structs\Product|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \Cashu\TPay\Catalogs\Structs\Product|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \Cashu\TPay\Catalogs\Structs\Product|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \Cashu\TPay\Catalogs\Structs\Product|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \Cashu\TPay\Catalogs\Structs\Product|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string product
     */
    public function getAttributeName()
    {
        return 'product';
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructArrayBase::__set_state()
     * @uses AbstractStructArrayBase::__set_state()
     * @param array $array the exported values
     * @return \Cashu\TPay\Catalogs\Arrays\ArrayOfproduct
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}

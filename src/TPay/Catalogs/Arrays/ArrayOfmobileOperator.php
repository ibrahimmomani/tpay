<?php

namespace Cashu\TPay\Catalogs\Arrays;

use \WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfmobileOperator Arrays
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfmobileOperator
 * @subpackage Arrays
 */
class ArrayOfmobileOperator extends AbstractStructArrayBase
{
    /**
     * The mobileOperator
     * Meta informations extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \Cashu\TPay\Catalogs\Structs\MobileOperator[]
     */
    public $mobileOperator;
    /**
     * Constructor method for ArrayOfmobileOperator
     * @uses ArrayOfmobileOperator::setMobileOperator()
     * @param \Cashu\TPay\Catalogs\Structs\MobileOperator[] $mobileOperator
     */
    public function __construct(array $mobileOperator = array())
    {
        $this
            ->setMobileOperator($mobileOperator);
    }
    /**
     * Get mobileOperator value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \Cashu\TPay\Catalogs\Structs\MobileOperator[]|null
     */
    public function getMobileOperator()
    {
        return isset($this->mobileOperator) ? $this->mobileOperator : null;
    }
    /**
     * Set mobileOperator value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \Cashu\TPay\Catalogs\Structs\MobileOperator[] $mobileOperator
     * @return \Cashu\TPay\Catalogs\Arrays\ArrayOfmobileOperator
     */
    public function setMobileOperator(array $mobileOperator = array())
    {
        foreach ($mobileOperator as $arrayOfmobileOperatorMobileOperatorItem) {
            // validation for constraint: itemType
            if (!$arrayOfmobileOperatorMobileOperatorItem instanceof \Cashu\TPay\Catalogs\Structs\MobileOperator) {
                throw new \InvalidArgumentException(sprintf('The mobileOperator property can only contain items of \Cashu\TPay\Catalogs\Structs\MobileOperator, "%s" given', is_object($arrayOfmobileOperatorMobileOperatorItem) ? get_class($arrayOfmobileOperatorMobileOperatorItem) : gettype($arrayOfmobileOperatorMobileOperatorItem)), __LINE__);
            }
        }
        if (is_null($mobileOperator) || (is_array($mobileOperator) && empty($mobileOperator))) {
            unset($this->mobileOperator);
        } else {
            $this->mobileOperator = $mobileOperator;
        }
        return $this;
    }
    /**
     * Add item to mobileOperator value
     * @throws \InvalidArgumentException
     * @param \Cashu\TPay\Catalogs\Structs\MobileOperator $item
     * @return \Cashu\TPay\Catalogs\Arrays\ArrayOfmobileOperator
     */
    public function addToMobileOperator(\Cashu\TPay\Catalogs\Structs\MobileOperator $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \Cashu\TPay\Catalogs\Structs\MobileOperator) {
            throw new \InvalidArgumentException(sprintf('The mobileOperator property can only contain items of \Cashu\TPay\Catalogs\Structs\MobileOperator, "%s" given', is_object($item) ? get_class($item) : gettype($item)), __LINE__);
        }
        $this->mobileOperator[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \Cashu\TPay\Catalogs\Structs\MobileOperator|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \Cashu\TPay\Catalogs\Structs\MobileOperator|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \Cashu\TPay\Catalogs\Structs\MobileOperator|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \Cashu\TPay\Catalogs\Structs\MobileOperator|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \Cashu\TPay\Catalogs\Structs\MobileOperator|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string mobileOperator
     */
    public function getAttributeName()
    {
        return 'mobileOperator';
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructArrayBase::__set_state()
     * @uses AbstractStructArrayBase::__set_state()
     * @param array $array the exported values
     * @return \Cashu\TPay\Catalogs\Arrays\ArrayOfmobileOperator
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}

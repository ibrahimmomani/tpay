<?php

namespace Cashu\TPay\Catalogs\Arrays;

use \WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfproductPricing Arrays
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfproductPricing
 * @subpackage Arrays
 */
class ArrayOfproductPricing extends AbstractStructArrayBase
{
    /**
     * The productPricing
     * Meta informations extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \Cashu\TPay\Catalogs\Structs\ProductPricing[]
     */
    public $productPricing;
    /**
     * Constructor method for ArrayOfproductPricing
     * @uses ArrayOfproductPricing::setProductPricing()
     * @param \Cashu\TPay\Catalogs\Structs\ProductPricing[] $productPricing
     */
    public function __construct(array $productPricing = array())
    {
        $this
            ->setProductPricing($productPricing);
    }
    /**
     * Get productPricing value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \Cashu\TPay\Catalogs\Structs\ProductPricing[]|null
     */
    public function getProductPricing()
    {
        return isset($this->productPricing) ? $this->productPricing : null;
    }
    /**
     * Set productPricing value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \Cashu\TPay\Catalogs\Structs\ProductPricing[] $productPricing
     * @return \Cashu\TPay\Catalogs\Arrays\ArrayOfproductPricing
     */
    public function setProductPricing(array $productPricing = array())
    {
        foreach ($productPricing as $arrayOfproductPricingProductPricingItem) {
            // validation for constraint: itemType
            if (!$arrayOfproductPricingProductPricingItem instanceof \Cashu\TPay\Catalogs\Structs\ProductPricing) {
                throw new \InvalidArgumentException(sprintf('The productPricing property can only contain items of \Cashu\TPay\Catalogs\Structs\ProductPricing, "%s" given', is_object($arrayOfproductPricingProductPricingItem) ? get_class($arrayOfproductPricingProductPricingItem) : gettype($arrayOfproductPricingProductPricingItem)), __LINE__);
            }
        }
        if (is_null($productPricing) || (is_array($productPricing) && empty($productPricing))) {
            unset($this->productPricing);
        } else {
            $this->productPricing = $productPricing;
        }
        return $this;
    }
    /**
     * Add item to productPricing value
     * @throws \InvalidArgumentException
     * @param \Cashu\TPay\Catalogs\Structs\ProductPricing $item
     * @return \Cashu\TPay\Catalogs\Arrays\ArrayOfproductPricing
     */
    public function addToProductPricing(\Cashu\TPay\Catalogs\Structs\ProductPricing $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \Cashu\TPay\Catalogs\Structs\ProductPricing) {
            throw new \InvalidArgumentException(sprintf('The productPricing property can only contain items of \Cashu\TPay\Catalogs\Structs\ProductPricing, "%s" given', is_object($item) ? get_class($item) : gettype($item)), __LINE__);
        }
        $this->productPricing[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \Cashu\TPay\Catalogs\Structs\ProductPricing|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \Cashu\TPay\Catalogs\Structs\ProductPricing|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \Cashu\TPay\Catalogs\Structs\ProductPricing|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \Cashu\TPay\Catalogs\Structs\ProductPricing|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \Cashu\TPay\Catalogs\Structs\ProductPricing|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string productPricing
     */
    public function getAttributeName()
    {
        return 'productPricing';
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructArrayBase::__set_state()
     * @uses AbstractStructArrayBase::__set_state()
     * @param array $array the exported values
     * @return \Cashu\TPay\Catalogs\Arrays\ArrayOfproductPricing
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}

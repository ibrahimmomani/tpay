<?php

namespace Cashu\TPay\Catalogs\Arrays;

use \WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfproductCatalog Arrays
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfproductCatalog
 * @subpackage Arrays
 */
class ArrayOfproductCatalog extends AbstractStructArrayBase
{
    /**
     * The productCatalog
     * Meta informations extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \Cashu\TPay\Catalogs\Structs\ProductCatalog[]
     */
    public $productCatalog;
    /**
     * Constructor method for ArrayOfproductCatalog
     * @uses ArrayOfproductCatalog::setProductCatalog()
     * @param \Cashu\TPay\Catalogs\Structs\ProductCatalog[] $productCatalog
     */
    public function __construct(array $productCatalog = array())
    {
        $this
            ->setProductCatalog($productCatalog);
    }
    /**
     * Get productCatalog value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \Cashu\TPay\Catalogs\Structs\ProductCatalog[]|null
     */
    public function getProductCatalog()
    {
        return isset($this->productCatalog) ? $this->productCatalog : null;
    }
    /**
     * Set productCatalog value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \Cashu\TPay\Catalogs\Structs\ProductCatalog[] $productCatalog
     * @return \Cashu\TPay\Catalogs\Arrays\ArrayOfproductCatalog
     */
    public function setProductCatalog(array $productCatalog = array())
    {
        foreach ($productCatalog as $arrayOfproductCatalogProductCatalogItem) {
            // validation for constraint: itemType
            if (!$arrayOfproductCatalogProductCatalogItem instanceof \Cashu\TPay\Catalogs\Structs\ProductCatalog) {
                throw new \InvalidArgumentException(sprintf('The productCatalog property can only contain items of \Cashu\TPay\Catalogs\Structs\ProductCatalog, "%s" given', is_object($arrayOfproductCatalogProductCatalogItem) ? get_class($arrayOfproductCatalogProductCatalogItem) : gettype($arrayOfproductCatalogProductCatalogItem)), __LINE__);
            }
        }
        if (is_null($productCatalog) || (is_array($productCatalog) && empty($productCatalog))) {
            unset($this->productCatalog);
        } else {
            $this->productCatalog = $productCatalog;
        }
        return $this;
    }
    /**
     * Add item to productCatalog value
     * @throws \InvalidArgumentException
     * @param \Cashu\TPay\Catalogs\Structs\ProductCatalog $item
     * @return \Cashu\TPay\Catalogs\Arrays\ArrayOfproductCatalog
     */
    public function addToProductCatalog(\Cashu\TPay\Catalogs\Structs\ProductCatalog $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \Cashu\TPay\Catalogs\Structs\ProductCatalog) {
            throw new \InvalidArgumentException(sprintf('The productCatalog property can only contain items of \Cashu\TPay\Catalogs\Structs\ProductCatalog, "%s" given', is_object($item) ? get_class($item) : gettype($item)), __LINE__);
        }
        $this->productCatalog[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \Cashu\TPay\Catalogs\Structs\ProductCatalog|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \Cashu\TPay\Catalogs\Structs\ProductCatalog|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \Cashu\TPay\Catalogs\Structs\ProductCatalog|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \Cashu\TPay\Catalogs\Structs\ProductCatalog|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \Cashu\TPay\Catalogs\Structs\ProductCatalog|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string productCatalog
     */
    public function getAttributeName()
    {
        return 'productCatalog';
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructArrayBase::__set_state()
     * @uses AbstractStructArrayBase::__set_state()
     * @param array $array the exported values
     * @return \Cashu\TPay\Catalogs\Arrays\ArrayOfproductCatalog
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}

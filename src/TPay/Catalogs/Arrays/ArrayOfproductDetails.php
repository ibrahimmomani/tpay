<?php

namespace Cashu\TPay\Catalogs\Arrays;

use \WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfproductDetails Arrays
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfproductDetails
 * @subpackage Arrays
 */
class ArrayOfproductDetails extends AbstractStructArrayBase
{
    /**
     * The productDetails
     * Meta informations extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \Cashu\TPay\Catalogs\Structs\ProductDetails[]
     */
    public $productDetails;
    /**
     * Constructor method for ArrayOfproductDetails
     * @uses ArrayOfproductDetails::setProductDetails()
     * @param \Cashu\TPay\Catalogs\Structs\ProductDetails[] $productDetails
     */
    public function __construct(array $productDetails = array())
    {
        $this
            ->setProductDetails($productDetails);
    }
    /**
     * Get productDetails value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \Cashu\TPay\Catalogs\Structs\ProductDetails[]|null
     */
    public function getProductDetails()
    {
        return isset($this->productDetails) ? $this->productDetails : null;
    }
    /**
     * Set productDetails value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \Cashu\TPay\Catalogs\Structs\ProductDetails[] $productDetails
     * @return \Cashu\TPay\Catalogs\Arrays\ArrayOfproductDetails
     */
    public function setProductDetails(array $productDetails = array())
    {
        foreach ($productDetails as $arrayOfproductDetailsProductDetailsItem) {
            // validation for constraint: itemType
            if (!$arrayOfproductDetailsProductDetailsItem instanceof \Cashu\TPay\Catalogs\Structs\ProductDetails) {
                throw new \InvalidArgumentException(sprintf('The productDetails property can only contain items of \Cashu\TPay\Catalogs\Structs\ProductDetails, "%s" given', is_object($arrayOfproductDetailsProductDetailsItem) ? get_class($arrayOfproductDetailsProductDetailsItem) : gettype($arrayOfproductDetailsProductDetailsItem)), __LINE__);
            }
        }
        if (is_null($productDetails) || (is_array($productDetails) && empty($productDetails))) {
            unset($this->productDetails);
        } else {
            $this->productDetails = $productDetails;
        }
        return $this;
    }
    /**
     * Add item to productDetails value
     * @throws \InvalidArgumentException
     * @param \Cashu\TPay\Catalogs\Structs\ProductDetails $item
     * @return \Cashu\TPay\Catalogs\Arrays\ArrayOfproductDetails
     */
    public function addToProductDetails(\Cashu\TPay\Catalogs\Structs\ProductDetails $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \Cashu\TPay\Catalogs\Structs\ProductDetails) {
            throw new \InvalidArgumentException(sprintf('The productDetails property can only contain items of \Cashu\TPay\Catalogs\Structs\ProductDetails, "%s" given', is_object($item) ? get_class($item) : gettype($item)), __LINE__);
        }
        $this->productDetails[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \Cashu\TPay\Catalogs\Structs\ProductDetails|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \Cashu\TPay\Catalogs\Structs\ProductDetails|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \Cashu\TPay\Catalogs\Structs\ProductDetails|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \Cashu\TPay\Catalogs\Structs\ProductDetails|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \Cashu\TPay\Catalogs\Structs\ProductDetails|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string productDetails
     */
    public function getAttributeName()
    {
        return 'productDetails';
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructArrayBase::__set_state()
     * @uses AbstractStructArrayBase::__set_state()
     * @param array $array the exported values
     * @return \Cashu\TPay\Catalogs\Arrays\ArrayOfproductDetails
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}

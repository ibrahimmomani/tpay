<?php

namespace Cashu\TPay\Catalogs\Enums;

/**
 * This class stands for MobilePaymentIntegrationMethod Enums
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:MobilePaymentIntegrationMethod
 * @subpackage Enumerations
 */
class MobilePaymentIntegrationMethod
{
    /**
     * Constant for value 'DirectBilling'
     * @return string 'DirectBilling'
     */
    const VALUE_DIRECT_BILLING = 'DirectBilling';
    /**
     * Constant for value 'MTPremiumSMS'
     * @return string 'MTPremiumSMS'
     */
    const VALUE_MTPREMIUM_SMS = 'MTPremiumSMS';
    /**
     * Constant for value 'MOPremiumSMS'
     * @return string 'MOPremiumSMS'
     */
    const VALUE_MOPREMIUM_SMS = 'MOPremiumSMS';
    /**
     * Return true if value is allowed
     * @uses self::getValidValues()
     * @param mixed $value value
     * @return bool true|false
     */
    public static function valueIsValid($value)
    {
        return ($value === null) || in_array($value, self::getValidValues(), true);
    }
    /**
     * Return allowed values
     * @uses self::VALUE_DIRECT_BILLING
     * @uses self::VALUE_MTPREMIUM_SMS
     * @uses self::VALUE_MOPREMIUM_SMS
     * @return string[]
     */
    public static function getValidValues()
    {
        return array(
            self::VALUE_DIRECT_BILLING,
            self::VALUE_MTPREMIUM_SMS,
            self::VALUE_MOPREMIUM_SMS,
        );
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}

<?php

namespace Cashu\TPay\Catalogs;

/**
 * Class which returns the class map definition
 * @package
 */
class ClassMap
{
    /**
     * Returns the mapping between the WSDL Structs and generated Structs' classes
     * This array is sent to the \SoapClient when calling the WS
     * @return string[]
     */
    final public static function get()
    {
        return array(
            'GetMobileOperators' => '\\Cashu\\TPay\\Catalogs\\Structs\\GetMobileOperators',
            'GetMobileOperatorsResponse' => '\\Cashu\\TPay\\Catalogs\\Structs\\GetMobileOperatorsResponse',
            'ArrayOfmobileOperator' => '\\Cashu\\TPay\\Catalogs\\Arrays\\ArrayOfmobileOperator',
            'mobileOperator' => '\\Cashu\\TPay\\Catalogs\\Structs\\MobileOperator',
            'catalogManagementFault' => '\\Cashu\\TPay\\Catalogs\\Structs\\CatalogManagementFault',
            'GetSalesChannels' => '\\Cashu\\TPay\\Catalogs\\Structs\\GetSalesChannels',
            'GetSalesChannelsResponse' => '\\Cashu\\TPay\\Catalogs\\Structs\\GetSalesChannelsResponse',
            'ArrayOfsalesChannel' => '\\Cashu\\TPay\\Catalogs\\Arrays\\ArrayOfsalesChannel',
            'salesChannel' => '\\Cashu\\TPay\\Catalogs\\Structs\\SalesChannel',
            'GetProductCatalogs' => '\\Cashu\\TPay\\Catalogs\\Structs\\GetProductCatalogs',
            'GetProductCatalogsResponse' => '\\Cashu\\TPay\\Catalogs\\Structs\\GetProductCatalogsResponse',
            'ArrayOfproductCatalog' => '\\Cashu\\TPay\\Catalogs\\Arrays\\ArrayOfproductCatalog',
            'productCatalog' => '\\Cashu\\TPay\\Catalogs\\Structs\\ProductCatalog',
            'GetProducts' => '\\Cashu\\TPay\\Catalogs\\Structs\\GetProducts',
            'GetProductsResponse' => '\\Cashu\\TPay\\Catalogs\\Structs\\GetProductsResponse',
            'ArrayOfproduct' => '\\Cashu\\TPay\\Catalogs\\Arrays\\ArrayOfproduct',
            'product' => '\\Cashu\\TPay\\Catalogs\\Structs\\Product',
            'ArrayOfproductPricing' => '\\Cashu\\TPay\\Catalogs\\Arrays\\ArrayOfproductPricing',
            'productPricing' => '\\Cashu\\TPay\\Catalogs\\Structs\\ProductPricing',
            'AddProductCatalog' => '\\Cashu\\TPay\\Catalogs\\Structs\\AddProductCatalog',
            'AddProductCatalogResponse' => '\\Cashu\\TPay\\Catalogs\\Structs\\AddProductCatalogResponse',
            'UpdateProductCatalog' => '\\Cashu\\TPay\\Catalogs\\Structs\\UpdateProductCatalog',
            'UpdateProductCatalogResponse' => '\\Cashu\\TPay\\Catalogs\\Structs\\UpdateProductCatalogResponse',
            'AddProduct' => '\\Cashu\\TPay\\Catalogs\\Structs\\AddProduct',
            'AddProductResponse' => '\\Cashu\\TPay\\Catalogs\\Structs\\AddProductResponse',
            'UpdateProduct' => '\\Cashu\\TPay\\Catalogs\\Structs\\UpdateProduct',
            'UpdateProductResponse' => '\\Cashu\\TPay\\Catalogs\\Structs\\UpdateProductResponse',
            'DeleteProduct' => '\\Cashu\\TPay\\Catalogs\\Structs\\DeleteProduct',
            'DeleteProductResponse' => '\\Cashu\\TPay\\Catalogs\\Structs\\DeleteProductResponse',
            'DeleteProductCatalog' => '\\Cashu\\TPay\\Catalogs\\Structs\\DeleteProductCatalog',
            'DeleteProductCatalogResponse' => '\\Cashu\\TPay\\Catalogs\\Structs\\DeleteProductCatalogResponse',
            'GetCatalogDetails' => '\\Cashu\\TPay\\Catalogs\\Structs\\GetCatalogDetails',
            'GetCatalogDetailsResponse' => '\\Cashu\\TPay\\Catalogs\\Structs\\GetCatalogDetailsResponse',
            'ArrayOfApiCatalog' => '\\Cashu\\TPay\\Catalogs\\Arrays\\ArrayOfApiCatalog',
            'ApiCatalog' => '\\Cashu\\TPay\\Catalogs\\Structs\\ApiCatalog',
            'GetCatalogProductDetails' => '\\Cashu\\TPay\\Catalogs\\Structs\\GetCatalogProductDetails',
            'GetCatalogProductDetailsResponse' => '\\Cashu\\TPay\\Catalogs\\Structs\\GetCatalogProductDetailsResponse',
            'productDetails' => '\\Cashu\\TPay\\Catalogs\\Structs\\ProductDetails',
            'GetCatalogProducts' => '\\Cashu\\TPay\\Catalogs\\Structs\\GetCatalogProducts',
            'GetCatalogProductsResponse' => '\\Cashu\\TPay\\Catalogs\\Structs\\GetCatalogProductsResponse',
            'ArrayOfproductDetails' => '\\Cashu\\TPay\\Catalogs\\Arrays\\ArrayOfproductDetails',
        );
    }
}

<?php

namespace Cashu\TPay\Catalogs\Structs;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for productDetails Structs
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:productDetails
 * @subpackage Structs
 */
class ProductDetails extends AbstractStructBase
{
    /**
     * The available
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var bool
     */
    public $available;
    /**
     * The catalogId
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $catalogId;
    /**
     * The catalogName
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $catalogName;
    /**
     * The currency
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $currency;
    /**
     * The id
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $id;
    /**
     * The image
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $image;
    /**
     * The price
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var float
     */
    public $price;
    /**
     * The productId
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $productId;
    /**
     * The productName
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $productName;
    /**
     * Constructor method for productDetails
     * @uses ProductDetails::setAvailable()
     * @uses ProductDetails::setCatalogId()
     * @uses ProductDetails::setCatalogName()
     * @uses ProductDetails::setCurrency()
     * @uses ProductDetails::setId()
     * @uses ProductDetails::setImage()
     * @uses ProductDetails::setPrice()
     * @uses ProductDetails::setProductId()
     * @uses ProductDetails::setProductName()
     * @param bool $available
     * @param int $catalogId
     * @param string $catalogName
     * @param string $currency
     * @param int $id
     * @param string $image
     * @param float $price
     * @param string $productId
     * @param string $productName
     */
    public function __construct($available = null, $catalogId = null, $catalogName = null, $currency = null, $id = null, $image = null, $price = null, $productId = null, $productName = null)
    {
        $this
            ->setAvailable($available)
            ->setCatalogId($catalogId)
            ->setCatalogName($catalogName)
            ->setCurrency($currency)
            ->setId($id)
            ->setImage($image)
            ->setPrice($price)
            ->setProductId($productId)
            ->setProductName($productName);
    }
    /**
     * Get available value
     * @return bool|null
     */
    public function getAvailable()
    {
        return $this->available;
    }
    /**
     * Set available value
     * @param bool $available
     * @return \Cashu\TPay\Catalogs\Structs\ProductDetails
     */
    public function setAvailable($available = null)
    {
        $this->available = $available;
        return $this;
    }
    /**
     * Get catalogId value
     * @return int|null
     */
    public function getCatalogId()
    {
        return $this->catalogId;
    }
    /**
     * Set catalogId value
     * @param int $catalogId
     * @return \Cashu\TPay\Catalogs\Structs\ProductDetails
     */
    public function setCatalogId($catalogId = null)
    {
        // validation for constraint: int
        if (!is_null($catalogId) && !is_numeric($catalogId)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a numeric value, "%s" given', gettype($catalogId)), __LINE__);
        }
        $this->catalogId = $catalogId;
        return $this;
    }
    /**
     * Get catalogName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCatalogName()
    {
        return isset($this->catalogName) ? $this->catalogName : null;
    }
    /**
     * Set catalogName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $catalogName
     * @return \Cashu\TPay\Catalogs\Structs\ProductDetails
     */
    public function setCatalogName($catalogName = null)
    {
        // validation for constraint: string
        if (!is_null($catalogName) && !is_string($catalogName)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($catalogName)), __LINE__);
        }
        if (is_null($catalogName) || (is_array($catalogName) && empty($catalogName))) {
            unset($this->catalogName);
        } else {
            $this->catalogName = $catalogName;
        }
        return $this;
    }
    /**
     * Get currency value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCurrency()
    {
        return isset($this->currency) ? $this->currency : null;
    }
    /**
     * Set currency value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $currency
     * @return \Cashu\TPay\Catalogs\Structs\ProductDetails
     */
    public function setCurrency($currency = null)
    {
        // validation for constraint: string
        if (!is_null($currency) && !is_string($currency)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($currency)), __LINE__);
        }
        if (is_null($currency) || (is_array($currency) && empty($currency))) {
            unset($this->currency);
        } else {
            $this->currency = $currency;
        }
        return $this;
    }
    /**
     * Get id value
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Set id value
     * @param int $id
     * @return \Cashu\TPay\Catalogs\Structs\ProductDetails
     */
    public function setId($id = null)
    {
        // validation for constraint: int
        if (!is_null($id) && !is_numeric($id)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a numeric value, "%s" given', gettype($id)), __LINE__);
        }
        $this->id = $id;
        return $this;
    }
    /**
     * Get image value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getImage()
    {
        return isset($this->image) ? $this->image : null;
    }
    /**
     * Set image value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $image
     * @return \Cashu\TPay\Catalogs\Structs\ProductDetails
     */
    public function setImage($image = null)
    {
        // validation for constraint: string
        if (!is_null($image) && !is_string($image)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($image)), __LINE__);
        }
        if (is_null($image) || (is_array($image) && empty($image))) {
            unset($this->image);
        } else {
            $this->image = $image;
        }
        return $this;
    }
    /**
     * Get price value
     * @return float|null
     */
    public function getPrice()
    {
        return $this->price;
    }
    /**
     * Set price value
     * @param float $price
     * @return \Cashu\TPay\Catalogs\Structs\ProductDetails
     */
    public function setPrice($price = null)
    {
        $this->price = $price;
        return $this;
    }
    /**
     * Get productId value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getProductId()
    {
        return isset($this->productId) ? $this->productId : null;
    }
    /**
     * Set productId value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $productId
     * @return \Cashu\TPay\Catalogs\Structs\ProductDetails
     */
    public function setProductId($productId = null)
    {
        // validation for constraint: string
        if (!is_null($productId) && !is_string($productId)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($productId)), __LINE__);
        }
        if (is_null($productId) || (is_array($productId) && empty($productId))) {
            unset($this->productId);
        } else {
            $this->productId = $productId;
        }
        return $this;
    }
    /**
     * Get productName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getProductName()
    {
        return isset($this->productName) ? $this->productName : null;
    }
    /**
     * Set productName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $productName
     * @return \Cashu\TPay\Catalogs\Structs\ProductDetails
     */
    public function setProductName($productName = null)
    {
        // validation for constraint: string
        if (!is_null($productName) && !is_string($productName)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($productName)), __LINE__);
        }
        if (is_null($productName) || (is_array($productName) && empty($productName))) {
            unset($this->productName);
        } else {
            $this->productName = $productName;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \Cashu\TPay\Catalogs\Structs\ProductDetails
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}

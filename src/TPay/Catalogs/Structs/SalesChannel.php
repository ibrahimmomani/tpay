<?php

namespace Cashu\TPay\Catalogs\Structs;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for salesChannel Structs
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:salesChannel
 * @subpackage Structs
 */
class SalesChannel extends AbstractStructBase
{
    /**
     * The currencyCode
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $currencyCode;
    /**
     * The isEnabled
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var bool
     */
    public $isEnabled;
    /**
     * The mobileOperatorCode
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $mobileOperatorCode;
    /**
     * The paymentMethod
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $paymentMethod;
    /**
     * The salesChannelId
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $salesChannelId;
    /**
     * Constructor method for salesChannel
     * @uses SalesChannel::setCurrencyCode()
     * @uses SalesChannel::setIsEnabled()
     * @uses SalesChannel::setMobileOperatorCode()
     * @uses SalesChannel::setPaymentMethod()
     * @uses SalesChannel::setSalesChannelId()
     * @param string $currencyCode
     * @param bool $isEnabled
     * @param string $mobileOperatorCode
     * @param string $paymentMethod
     * @param int $salesChannelId
     */
    public function __construct($currencyCode = null, $isEnabled = null, $mobileOperatorCode = null, $paymentMethod = null, $salesChannelId = null)
    {
        $this
            ->setCurrencyCode($currencyCode)
            ->setIsEnabled($isEnabled)
            ->setMobileOperatorCode($mobileOperatorCode)
            ->setPaymentMethod($paymentMethod)
            ->setSalesChannelId($salesChannelId);
    }
    /**
     * Get currencyCode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCurrencyCode()
    {
        return isset($this->currencyCode) ? $this->currencyCode : null;
    }
    /**
     * Set currencyCode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $currencyCode
     * @return \Cashu\TPay\Catalogs\Structs\SalesChannel
     */
    public function setCurrencyCode($currencyCode = null)
    {
        // validation for constraint: string
        if (!is_null($currencyCode) && !is_string($currencyCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($currencyCode)), __LINE__);
        }
        if (is_null($currencyCode) || (is_array($currencyCode) && empty($currencyCode))) {
            unset($this->currencyCode);
        } else {
            $this->currencyCode = $currencyCode;
        }
        return $this;
    }
    /**
     * Get isEnabled value
     * @return bool|null
     */
    public function getIsEnabled()
    {
        return $this->isEnabled;
    }
    /**
     * Set isEnabled value
     * @param bool $isEnabled
     * @return \Cashu\TPay\Catalogs\Structs\SalesChannel
     */
    public function setIsEnabled($isEnabled = null)
    {
        $this->isEnabled = $isEnabled;
        return $this;
    }
    /**
     * Get mobileOperatorCode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getMobileOperatorCode()
    {
        return isset($this->mobileOperatorCode) ? $this->mobileOperatorCode : null;
    }
    /**
     * Set mobileOperatorCode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $mobileOperatorCode
     * @return \Cashu\TPay\Catalogs\Structs\SalesChannel
     */
    public function setMobileOperatorCode($mobileOperatorCode = null)
    {
        // validation for constraint: string
        if (!is_null($mobileOperatorCode) && !is_string($mobileOperatorCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($mobileOperatorCode)), __LINE__);
        }
        if (is_null($mobileOperatorCode) || (is_array($mobileOperatorCode) && empty($mobileOperatorCode))) {
            unset($this->mobileOperatorCode);
        } else {
            $this->mobileOperatorCode = $mobileOperatorCode;
        }
        return $this;
    }
    /**
     * Get paymentMethod value
     * @return string|null
     */
    public function getPaymentMethod()
    {
        return $this->paymentMethod;
    }
    /**
     * Set paymentMethod value
     * @uses \Cashu\TPay\Catalogs\Enums\MobilePaymentIntegrationMethod::valueIsValid()
     * @uses \Cashu\TPay\Catalogs\Enums\MobilePaymentIntegrationMethod::getValidValues()
     * @throws \InvalidArgumentException
     * @param string $paymentMethod
     * @return \Cashu\TPay\Catalogs\Structs\SalesChannel
     */
    public function setPaymentMethod($paymentMethod = null)
    {
        // validation for constraint: enumeration
        if (!\Cashu\TPay\Catalogs\Enums\MobilePaymentIntegrationMethod::valueIsValid($paymentMethod)) {
            throw new \InvalidArgumentException(sprintf('Value "%s" is invalid, please use one of: %s', $paymentMethod, implode(', ', \Cashu\TPay\Catalogs\Enums\MobilePaymentIntegrationMethod::getValidValues())), __LINE__);
        }
        $this->paymentMethod = $paymentMethod;
        return $this;
    }
    /**
     * Get salesChannelId value
     * @return int|null
     */
    public function getSalesChannelId()
    {
        return $this->salesChannelId;
    }
    /**
     * Set salesChannelId value
     * @param int $salesChannelId
     * @return \Cashu\TPay\Catalogs\Structs\SalesChannel
     */
    public function setSalesChannelId($salesChannelId = null)
    {
        // validation for constraint: int
        if (!is_null($salesChannelId) && !is_numeric($salesChannelId)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a numeric value, "%s" given', gettype($salesChannelId)), __LINE__);
        }
        $this->salesChannelId = $salesChannelId;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \Cashu\TPay\Catalogs\Structs\SalesChannel
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}

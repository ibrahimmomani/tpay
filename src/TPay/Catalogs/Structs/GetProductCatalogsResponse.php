<?php

namespace Cashu\TPay\Catalogs\Structs;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetProductCatalogsResponse Structs
 * @subpackage Structs
 */
class GetProductCatalogsResponse extends AbstractStructBase
{
    /**
     * The GetProductCatalogsResult
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \Cashu\TPay\Catalogs\Arrays\ArrayOfproductCatalog
     */
    public $GetProductCatalogsResult;
    /**
     * Constructor method for GetProductCatalogsResponse
     * @uses GetProductCatalogsResponse::setGetProductCatalogsResult()
     * @param \Cashu\TPay\Catalogs\Arrays\ArrayOfproductCatalog $getProductCatalogsResult
     */
    public function __construct(\Cashu\TPay\Catalogs\Arrays\ArrayOfproductCatalog $getProductCatalogsResult = null)
    {
        $this
            ->setGetProductCatalogsResult($getProductCatalogsResult);
    }
    /**
     * Get GetProductCatalogsResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \Cashu\TPay\Catalogs\Arrays\ArrayOfproductCatalog|null
     */
    public function getGetProductCatalogsResult()
    {
        return isset($this->GetProductCatalogsResult) ? $this->GetProductCatalogsResult : null;
    }
    /**
     * Set GetProductCatalogsResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \Cashu\TPay\Catalogs\Arrays\ArrayOfproductCatalog $getProductCatalogsResult
     * @return \Cashu\TPay\Catalogs\Structs\GetProductCatalogsResponse
     */
    public function setGetProductCatalogsResult(\Cashu\TPay\Catalogs\Arrays\ArrayOfproductCatalog $getProductCatalogsResult = null)
    {
        if (is_null($getProductCatalogsResult) || (is_array($getProductCatalogsResult) && empty($getProductCatalogsResult))) {
            unset($this->GetProductCatalogsResult);
        } else {
            $this->GetProductCatalogsResult = $getProductCatalogsResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \Cashu\TPay\Catalogs\Structs\GetProductCatalogsResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}

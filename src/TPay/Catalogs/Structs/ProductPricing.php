<?php

namespace Cashu\TPay\Catalogs\Structs;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for productPricing Structs
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:productPricing
 * @subpackage Structs
 */
class ProductPricing extends AbstractStructBase
{
    /**
     * The catalogId
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $catalogId;
    /**
     * The price
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var float
     */
    public $price;
    /**
     * Constructor method for productPricing
     * @uses ProductPricing::setCatalogId()
     * @uses ProductPricing::setPrice()
     * @param int $catalogId
     * @param float $price
     */
    public function __construct($catalogId = null, $price = null)
    {
        $this
            ->setCatalogId($catalogId)
            ->setPrice($price);
    }
    /**
     * Get catalogId value
     * @return int|null
     */
    public function getCatalogId()
    {
        return $this->catalogId;
    }
    /**
     * Set catalogId value
     * @param int $catalogId
     * @return \Cashu\TPay\Catalogs\Structs\ProductPricing
     */
    public function setCatalogId($catalogId = null)
    {
        // validation for constraint: int
        if (!is_null($catalogId) && !is_numeric($catalogId)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a numeric value, "%s" given', gettype($catalogId)), __LINE__);
        }
        $this->catalogId = $catalogId;
        return $this;
    }
    /**
     * Get price value
     * @return float|null
     */
    public function getPrice()
    {
        return $this->price;
    }
    /**
     * Set price value
     * @param float $price
     * @return \Cashu\TPay\Catalogs\Structs\ProductPricing
     */
    public function setPrice($price = null)
    {
        $this->price = $price;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \Cashu\TPay\Catalogs\Structs\ProductPricing
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}

<?php

namespace Cashu\TPay\Catalogs\Structs;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetCatalogProductsResponse Structs
 * @subpackage Structs
 */
class GetCatalogProductsResponse extends AbstractStructBase
{
    /**
     * The GetCatalogProductsResult
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \Cashu\TPay\Catalogs\Arrays\ArrayOfproductDetails
     */
    public $GetCatalogProductsResult;
    /**
     * Constructor method for GetCatalogProductsResponse
     * @uses GetCatalogProductsResponse::setGetCatalogProductsResult()
     * @param \Cashu\TPay\Catalogs\Arrays\ArrayOfproductDetails $getCatalogProductsResult
     */
    public function __construct(\Cashu\TPay\Catalogs\Arrays\ArrayOfproductDetails $getCatalogProductsResult = null)
    {
        $this
            ->setGetCatalogProductsResult($getCatalogProductsResult);
    }
    /**
     * Get GetCatalogProductsResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \Cashu\TPay\Catalogs\Arrays\ArrayOfproductDetails|null
     */
    public function getGetCatalogProductsResult()
    {
        return isset($this->GetCatalogProductsResult) ? $this->GetCatalogProductsResult : null;
    }
    /**
     * Set GetCatalogProductsResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \Cashu\TPay\Catalogs\Arrays\ArrayOfproductDetails $getCatalogProductsResult
     * @return \Cashu\TPay\Catalogs\Structs\GetCatalogProductsResponse
     */
    public function setGetCatalogProductsResult(\Cashu\TPay\Catalogs\Arrays\ArrayOfproductDetails $getCatalogProductsResult = null)
    {
        if (is_null($getCatalogProductsResult) || (is_array($getCatalogProductsResult) && empty($getCatalogProductsResult))) {
            unset($this->GetCatalogProductsResult);
        } else {
            $this->GetCatalogProductsResult = $getCatalogProductsResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \Cashu\TPay\Catalogs\Structs\GetCatalogProductsResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}

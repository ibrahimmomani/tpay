<?php

namespace Cashu\TPay\Catalogs\Structs;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for DeleteProduct Structs
 * @subpackage Structs
 */
class DeleteProduct extends AbstractStructBase
{
    /**
     * The productId
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $productId;
    /**
     * The signature
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $signature;
    /**
     * Constructor method for DeleteProduct
     * @uses DeleteProduct::setProductId()
     * @uses DeleteProduct::setSignature()
     * @param int $productId
     * @param string $signature
     */
    public function __construct($productId = null, $signature = null)
    {
        $this
            ->setProductId($productId)
            ->setSignature($signature);
    }
    /**
     * Get productId value
     * @return int|null
     */
    public function getProductId()
    {
        return $this->productId;
    }
    /**
     * Set productId value
     * @param int $productId
     * @return \Cashu\TPay\Catalogs\Structs\DeleteProduct
     */
    public function setProductId($productId = null)
    {
        // validation for constraint: int
        if (!is_null($productId) && !is_numeric($productId)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a numeric value, "%s" given', gettype($productId)), __LINE__);
        }
        $this->productId = $productId;
        return $this;
    }
    /**
     * Get signature value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSignature()
    {
        return isset($this->signature) ? $this->signature : null;
    }
    /**
     * Set signature value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $signature
     * @return \Cashu\TPay\Catalogs\Structs\DeleteProduct
     */
    public function setSignature($signature = null)
    {
        // validation for constraint: string
        if (!is_null($signature) && !is_string($signature)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($signature)), __LINE__);
        }
        if (is_null($signature) || (is_array($signature) && empty($signature))) {
            unset($this->signature);
        } else {
            $this->signature = $signature;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \Cashu\TPay\Catalogs\Structs\DeleteProduct
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}

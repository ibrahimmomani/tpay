<?php

namespace Cashu\TPay\Catalogs\Structs;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetSalesChannelsResponse Structs
 * @subpackage Structs
 */
class GetSalesChannelsResponse extends AbstractStructBase
{
    /**
     * The GetSalesChannelsResult
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \Cashu\TPay\Catalogs\Arrays\ArrayOfsalesChannel
     */
    public $GetSalesChannelsResult;
    /**
     * Constructor method for GetSalesChannelsResponse
     * @uses GetSalesChannelsResponse::setGetSalesChannelsResult()
     * @param \Cashu\TPay\Catalogs\Arrays\ArrayOfsalesChannel $getSalesChannelsResult
     */
    public function __construct(\Cashu\TPay\Catalogs\Arrays\ArrayOfsalesChannel $getSalesChannelsResult = null)
    {
        $this
            ->setGetSalesChannelsResult($getSalesChannelsResult);
    }
    /**
     * Get GetSalesChannelsResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \Cashu\TPay\Catalogs\Arrays\ArrayOfsalesChannel|null
     */
    public function getGetSalesChannelsResult()
    {
        return isset($this->GetSalesChannelsResult) ? $this->GetSalesChannelsResult : null;
    }
    /**
     * Set GetSalesChannelsResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \Cashu\TPay\Catalogs\Arrays\ArrayOfsalesChannel $getSalesChannelsResult
     * @return \Cashu\TPay\Catalogs\Structs\GetSalesChannelsResponse
     */
    public function setGetSalesChannelsResult(\Cashu\TPay\Catalogs\Arrays\ArrayOfsalesChannel $getSalesChannelsResult = null)
    {
        if (is_null($getSalesChannelsResult) || (is_array($getSalesChannelsResult) && empty($getSalesChannelsResult))) {
            unset($this->GetSalesChannelsResult);
        } else {
            $this->GetSalesChannelsResult = $getSalesChannelsResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \Cashu\TPay\Catalogs\Structs\GetSalesChannelsResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}

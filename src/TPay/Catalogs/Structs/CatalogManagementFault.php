<?php

namespace Cashu\TPay\Catalogs\Structs;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for catalogManagementFault Structs
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:catalogManagementFault
 * @subpackage Structs
 */
class CatalogManagementFault extends AbstractStructBase
{
    /**
     * The errorMessage
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $errorMessage;
    /**
     * The parameterName
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $parameterName;
    /**
     * Constructor method for catalogManagementFault
     * @uses CatalogManagementFault::setErrorMessage()
     * @uses CatalogManagementFault::setParameterName()
     * @param string $errorMessage
     * @param string $parameterName
     */
    public function __construct($errorMessage = null, $parameterName = null)
    {
        $this
            ->setErrorMessage($errorMessage)
            ->setParameterName($parameterName);
    }
    /**
     * Get errorMessage value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getErrorMessage()
    {
        return isset($this->errorMessage) ? $this->errorMessage : null;
    }
    /**
     * Set errorMessage value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $errorMessage
     * @return \Cashu\TPay\Catalogs\Structs\CatalogManagementFault
     */
    public function setErrorMessage($errorMessage = null)
    {
        // validation for constraint: string
        if (!is_null($errorMessage) && !is_string($errorMessage)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($errorMessage)), __LINE__);
        }
        if (is_null($errorMessage) || (is_array($errorMessage) && empty($errorMessage))) {
            unset($this->errorMessage);
        } else {
            $this->errorMessage = $errorMessage;
        }
        return $this;
    }
    /**
     * Get parameterName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getParameterName()
    {
        return isset($this->parameterName) ? $this->parameterName : null;
    }
    /**
     * Set parameterName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $parameterName
     * @return \Cashu\TPay\Catalogs\Structs\CatalogManagementFault
     */
    public function setParameterName($parameterName = null)
    {
        // validation for constraint: string
        if (!is_null($parameterName) && !is_string($parameterName)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($parameterName)), __LINE__);
        }
        if (is_null($parameterName) || (is_array($parameterName) && empty($parameterName))) {
            unset($this->parameterName);
        } else {
            $this->parameterName = $parameterName;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \Cashu\TPay\Catalogs\Structs\CatalogManagementFault
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}

<?php

namespace Cashu\TPay\Catalogs\Structs;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetCatalogDetails Structs
 * @subpackage Structs
 */
class GetCatalogDetails extends AbstractStructBase
{
    /**
     * The catalogName
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $catalogName;
    /**
     * The productId
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $productId;
    /**
     * The signature
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $signature;
    /**
     * Constructor method for GetCatalogDetails
     * @uses GetCatalogDetails::setCatalogName()
     * @uses GetCatalogDetails::setProductId()
     * @uses GetCatalogDetails::setSignature()
     * @param string $catalogName
     * @param string $productId
     * @param string $signature
     */
    public function __construct($catalogName = null, $productId = null, $signature = null)
    {
        $this
            ->setCatalogName($catalogName)
            ->setProductId($productId)
            ->setSignature($signature);
    }
    /**
     * Get catalogName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCatalogName()
    {
        return isset($this->catalogName) ? $this->catalogName : null;
    }
    /**
     * Set catalogName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $catalogName
     * @return \Cashu\TPay\Catalogs\Structs\GetCatalogDetails
     */
    public function setCatalogName($catalogName = null)
    {
        // validation for constraint: string
        if (!is_null($catalogName) && !is_string($catalogName)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($catalogName)), __LINE__);
        }
        if (is_null($catalogName) || (is_array($catalogName) && empty($catalogName))) {
            unset($this->catalogName);
        } else {
            $this->catalogName = $catalogName;
        }
        return $this;
    }
    /**
     * Get productId value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getProductId()
    {
        return isset($this->productId) ? $this->productId : null;
    }
    /**
     * Set productId value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $productId
     * @return \Cashu\TPay\Catalogs\Structs\GetCatalogDetails
     */
    public function setProductId($productId = null)
    {
        // validation for constraint: string
        if (!is_null($productId) && !is_string($productId)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($productId)), __LINE__);
        }
        if (is_null($productId) || (is_array($productId) && empty($productId))) {
            unset($this->productId);
        } else {
            $this->productId = $productId;
        }
        return $this;
    }
    /**
     * Get signature value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSignature()
    {
        return isset($this->signature) ? $this->signature : null;
    }
    /**
     * Set signature value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $signature
     * @return \Cashu\TPay\Catalogs\Structs\GetCatalogDetails
     */
    public function setSignature($signature = null)
    {
        // validation for constraint: string
        if (!is_null($signature) && !is_string($signature)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($signature)), __LINE__);
        }
        if (is_null($signature) || (is_array($signature) && empty($signature))) {
            unset($this->signature);
        } else {
            $this->signature = $signature;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \Cashu\TPay\Catalogs\Structs\GetCatalogDetails
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}

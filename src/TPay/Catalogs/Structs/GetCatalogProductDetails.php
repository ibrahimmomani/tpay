<?php

namespace Cashu\TPay\Catalogs\Structs;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetCatalogProductDetails Structs
 * @subpackage Structs
 */
class GetCatalogProductDetails extends AbstractStructBase
{
    /**
     * The catalogId
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $catalogId;
    /**
     * The productId
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $productId;
    /**
     * The currentDate
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $currentDate;
    /**
     * The signature
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $signature;
    /**
     * Constructor method for GetCatalogProductDetails
     * @uses GetCatalogProductDetails::setCatalogId()
     * @uses GetCatalogProductDetails::setProductId()
     * @uses GetCatalogProductDetails::setCurrentDate()
     * @uses GetCatalogProductDetails::setSignature()
     * @param int $catalogId
     * @param string $productId
     * @param string $currentDate
     * @param string $signature
     */
    public function __construct($catalogId = null, $productId = null, $currentDate = null, $signature = null)
    {
        $this
            ->setCatalogId($catalogId)
            ->setProductId($productId)
            ->setCurrentDate($currentDate)
            ->setSignature($signature);
    }
    /**
     * Get catalogId value
     * @return int|null
     */
    public function getCatalogId()
    {
        return $this->catalogId;
    }
    /**
     * Set catalogId value
     * @param int $catalogId
     * @return \Cashu\TPay\Catalogs\Structs\GetCatalogProductDetails
     */
    public function setCatalogId($catalogId = null)
    {
        // validation for constraint: int
        if (!is_null($catalogId) && !is_numeric($catalogId)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a numeric value, "%s" given', gettype($catalogId)), __LINE__);
        }
        $this->catalogId = $catalogId;
        return $this;
    }
    /**
     * Get productId value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getProductId()
    {
        return isset($this->productId) ? $this->productId : null;
    }
    /**
     * Set productId value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $productId
     * @return \Cashu\TPay\Catalogs\Structs\GetCatalogProductDetails
     */
    public function setProductId($productId = null)
    {
        // validation for constraint: string
        if (!is_null($productId) && !is_string($productId)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($productId)), __LINE__);
        }
        if (is_null($productId) || (is_array($productId) && empty($productId))) {
            unset($this->productId);
        } else {
            $this->productId = $productId;
        }
        return $this;
    }
    /**
     * Get currentDate value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCurrentDate()
    {
        return isset($this->currentDate) ? $this->currentDate : null;
    }
    /**
     * Set currentDate value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $currentDate
     * @return \Cashu\TPay\Catalogs\Structs\GetCatalogProductDetails
     */
    public function setCurrentDate($currentDate = null)
    {
        // validation for constraint: string
        if (!is_null($currentDate) && !is_string($currentDate)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($currentDate)), __LINE__);
        }
        if (is_null($currentDate) || (is_array($currentDate) && empty($currentDate))) {
            unset($this->currentDate);
        } else {
            $this->currentDate = $currentDate;
        }
        return $this;
    }
    /**
     * Get signature value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSignature()
    {
        return isset($this->signature) ? $this->signature : null;
    }
    /**
     * Set signature value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $signature
     * @return \Cashu\TPay\Catalogs\Structs\GetCatalogProductDetails
     */
    public function setSignature($signature = null)
    {
        // validation for constraint: string
        if (!is_null($signature) && !is_string($signature)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($signature)), __LINE__);
        }
        if (is_null($signature) || (is_array($signature) && empty($signature))) {
            unset($this->signature);
        } else {
            $this->signature = $signature;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \Cashu\TPay\Catalogs\Structs\GetCatalogProductDetails
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}

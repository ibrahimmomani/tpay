<?php

namespace Cashu\TPay\Catalogs\Structs;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetMobileOperatorsResponse Structs
 * @subpackage Structs
 */
class GetMobileOperatorsResponse extends AbstractStructBase
{
    /**
     * The GetMobileOperatorsResult
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \Cashu\TPay\Catalogs\Arrays\ArrayOfmobileOperator
     */
    public $GetMobileOperatorsResult;
    /**
     * Constructor method for GetMobileOperatorsResponse
     * @uses GetMobileOperatorsResponse::setGetMobileOperatorsResult()
     * @param \Cashu\TPay\Catalogs\Arrays\ArrayOfmobileOperator $getMobileOperatorsResult
     */
    public function __construct(\Cashu\TPay\Catalogs\Arrays\ArrayOfmobileOperator $getMobileOperatorsResult = null)
    {
        $this
            ->setGetMobileOperatorsResult($getMobileOperatorsResult);
    }
    /**
     * Get GetMobileOperatorsResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \Cashu\TPay\Catalogs\Arrays\ArrayOfmobileOperator|null
     */
    public function getGetMobileOperatorsResult()
    {
        return isset($this->GetMobileOperatorsResult) ? $this->GetMobileOperatorsResult : null;
    }
    /**
     * Set GetMobileOperatorsResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \Cashu\TPay\Catalogs\Arrays\ArrayOfmobileOperator $getMobileOperatorsResult
     * @return \Cashu\TPay\Catalogs\Structs\GetMobileOperatorsResponse
     */
    public function setGetMobileOperatorsResult(\Cashu\TPay\Catalogs\Arrays\ArrayOfmobileOperator $getMobileOperatorsResult = null)
    {
        if (is_null($getMobileOperatorsResult) || (is_array($getMobileOperatorsResult) && empty($getMobileOperatorsResult))) {
            unset($this->GetMobileOperatorsResult);
        } else {
            $this->GetMobileOperatorsResult = $getMobileOperatorsResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \Cashu\TPay\Catalogs\Structs\GetMobileOperatorsResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}

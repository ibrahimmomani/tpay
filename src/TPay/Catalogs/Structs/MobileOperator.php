<?php

namespace Cashu\TPay\Catalogs\Structs;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for mobileOperator Structs
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:mobileOperator
 * @subpackage Structs
 */
class MobileOperator extends AbstractStructBase
{
    /**
     * The countryCode
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $countryCode;
    /**
     * The countryName
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $countryName;
    /**
     * The currencyCode
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $currencyCode;
    /**
     * The operatorCode
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $operatorCode;
    /**
     * The operatorName
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $operatorName;
    /**
     * Constructor method for mobileOperator
     * @uses MobileOperator::setCountryCode()
     * @uses MobileOperator::setCountryName()
     * @uses MobileOperator::setCurrencyCode()
     * @uses MobileOperator::setOperatorCode()
     * @uses MobileOperator::setOperatorName()
     * @param string $countryCode
     * @param string $countryName
     * @param string $currencyCode
     * @param string $operatorCode
     * @param string $operatorName
     */
    public function __construct($countryCode = null, $countryName = null, $currencyCode = null, $operatorCode = null, $operatorName = null)
    {
        $this
            ->setCountryCode($countryCode)
            ->setCountryName($countryName)
            ->setCurrencyCode($currencyCode)
            ->setOperatorCode($operatorCode)
            ->setOperatorName($operatorName);
    }
    /**
     * Get countryCode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCountryCode()
    {
        return isset($this->countryCode) ? $this->countryCode : null;
    }
    /**
     * Set countryCode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $countryCode
     * @return \Cashu\TPay\Catalogs\Structs\MobileOperator
     */
    public function setCountryCode($countryCode = null)
    {
        // validation for constraint: string
        if (!is_null($countryCode) && !is_string($countryCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($countryCode)), __LINE__);
        }
        if (is_null($countryCode) || (is_array($countryCode) && empty($countryCode))) {
            unset($this->countryCode);
        } else {
            $this->countryCode = $countryCode;
        }
        return $this;
    }
    /**
     * Get countryName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCountryName()
    {
        return isset($this->countryName) ? $this->countryName : null;
    }
    /**
     * Set countryName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $countryName
     * @return \Cashu\TPay\Catalogs\Structs\MobileOperator
     */
    public function setCountryName($countryName = null)
    {
        // validation for constraint: string
        if (!is_null($countryName) && !is_string($countryName)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($countryName)), __LINE__);
        }
        if (is_null($countryName) || (is_array($countryName) && empty($countryName))) {
            unset($this->countryName);
        } else {
            $this->countryName = $countryName;
        }
        return $this;
    }
    /**
     * Get currencyCode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCurrencyCode()
    {
        return isset($this->currencyCode) ? $this->currencyCode : null;
    }
    /**
     * Set currencyCode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $currencyCode
     * @return \Cashu\TPay\Catalogs\Structs\MobileOperator
     */
    public function setCurrencyCode($currencyCode = null)
    {
        // validation for constraint: string
        if (!is_null($currencyCode) && !is_string($currencyCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($currencyCode)), __LINE__);
        }
        if (is_null($currencyCode) || (is_array($currencyCode) && empty($currencyCode))) {
            unset($this->currencyCode);
        } else {
            $this->currencyCode = $currencyCode;
        }
        return $this;
    }
    /**
     * Get operatorCode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getOperatorCode()
    {
        return isset($this->operatorCode) ? $this->operatorCode : null;
    }
    /**
     * Set operatorCode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $operatorCode
     * @return \Cashu\TPay\Catalogs\Structs\MobileOperator
     */
    public function setOperatorCode($operatorCode = null)
    {
        // validation for constraint: string
        if (!is_null($operatorCode) && !is_string($operatorCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($operatorCode)), __LINE__);
        }
        if (is_null($operatorCode) || (is_array($operatorCode) && empty($operatorCode))) {
            unset($this->operatorCode);
        } else {
            $this->operatorCode = $operatorCode;
        }
        return $this;
    }
    /**
     * Get operatorName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getOperatorName()
    {
        return isset($this->operatorName) ? $this->operatorName : null;
    }
    /**
     * Set operatorName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $operatorName
     * @return \Cashu\TPay\Catalogs\Structs\MobileOperator
     */
    public function setOperatorName($operatorName = null)
    {
        // validation for constraint: string
        if (!is_null($operatorName) && !is_string($operatorName)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($operatorName)), __LINE__);
        }
        if (is_null($operatorName) || (is_array($operatorName) && empty($operatorName))) {
            unset($this->operatorName);
        } else {
            $this->operatorName = $operatorName;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \Cashu\TPay\Catalogs\Structs\MobileOperator
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}

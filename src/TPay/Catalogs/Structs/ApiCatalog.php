<?php

namespace Cashu\TPay\Catalogs\Structs;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ApiCatalog Structs
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:ApiCatalog
 * @subpackage Structs
 */
class ApiCatalog extends AbstractStructBase
{
    /**
     * The catalogDescription
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $catalogDescription;
    /**
     * The catalogId
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $catalogId;
    /**
     * The catalogName
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $catalogName;
    /**
     * The isEnabled
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var bool
     */
    public $isEnabled;
    /**
     * The mobileOperator
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \Cashu\TPay\Catalogs\Structs\MobileOperator
     */
    public $mobileOperator;
    /**
     * The products
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \Cashu\TPay\Catalogs\Arrays\ArrayOfproduct
     */
    public $products;
    /**
     * Constructor method for ApiCatalog
     * @uses ApiCatalog::setCatalogDescription()
     * @uses ApiCatalog::setCatalogId()
     * @uses ApiCatalog::setCatalogName()
     * @uses ApiCatalog::setIsEnabled()
     * @uses ApiCatalog::setMobileOperator()
     * @uses ApiCatalog::setProducts()
     * @param string $catalogDescription
     * @param int $catalogId
     * @param string $catalogName
     * @param bool $isEnabled
     * @param \Cashu\TPay\Catalogs\Structs\MobileOperator $mobileOperator
     * @param \Cashu\TPay\Catalogs\Arrays\ArrayOfproduct $products
     */
    public function __construct($catalogDescription = null, $catalogId = null, $catalogName = null, $isEnabled = null, \Cashu\TPay\Catalogs\Structs\MobileOperator $mobileOperator = null, \Cashu\TPay\Catalogs\Arrays\ArrayOfproduct $products = null)
    {
        $this
            ->setCatalogDescription($catalogDescription)
            ->setCatalogId($catalogId)
            ->setCatalogName($catalogName)
            ->setIsEnabled($isEnabled)
            ->setMobileOperator($mobileOperator)
            ->setProducts($products);
    }
    /**
     * Get catalogDescription value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCatalogDescription()
    {
        return isset($this->catalogDescription) ? $this->catalogDescription : null;
    }
    /**
     * Set catalogDescription value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $catalogDescription
     * @return \Cashu\TPay\Catalogs\Structs\ApiCatalog
     */
    public function setCatalogDescription($catalogDescription = null)
    {
        // validation for constraint: string
        if (!is_null($catalogDescription) && !is_string($catalogDescription)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($catalogDescription)), __LINE__);
        }
        if (is_null($catalogDescription) || (is_array($catalogDescription) && empty($catalogDescription))) {
            unset($this->catalogDescription);
        } else {
            $this->catalogDescription = $catalogDescription;
        }
        return $this;
    }
    /**
     * Get catalogId value
     * @return int|null
     */
    public function getCatalogId()
    {
        return $this->catalogId;
    }
    /**
     * Set catalogId value
     * @param int $catalogId
     * @return \Cashu\TPay\Catalogs\Structs\ApiCatalog
     */
    public function setCatalogId($catalogId = null)
    {
        // validation for constraint: int
        if (!is_null($catalogId) && !is_numeric($catalogId)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a numeric value, "%s" given', gettype($catalogId)), __LINE__);
        }
        $this->catalogId = $catalogId;
        return $this;
    }
    /**
     * Get catalogName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCatalogName()
    {
        return isset($this->catalogName) ? $this->catalogName : null;
    }
    /**
     * Set catalogName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $catalogName
     * @return \Cashu\TPay\Catalogs\Structs\ApiCatalog
     */
    public function setCatalogName($catalogName = null)
    {
        // validation for constraint: string
        if (!is_null($catalogName) && !is_string($catalogName)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($catalogName)), __LINE__);
        }
        if (is_null($catalogName) || (is_array($catalogName) && empty($catalogName))) {
            unset($this->catalogName);
        } else {
            $this->catalogName = $catalogName;
        }
        return $this;
    }
    /**
     * Get isEnabled value
     * @return bool|null
     */
    public function getIsEnabled()
    {
        return $this->isEnabled;
    }
    /**
     * Set isEnabled value
     * @param bool $isEnabled
     * @return \Cashu\TPay\Catalogs\Structs\ApiCatalog
     */
    public function setIsEnabled($isEnabled = null)
    {
        $this->isEnabled = $isEnabled;
        return $this;
    }
    /**
     * Get mobileOperator value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \Cashu\TPay\Catalogs\Structs\MobileOperator|null
     */
    public function getMobileOperator()
    {
        return isset($this->mobileOperator) ? $this->mobileOperator : null;
    }
    /**
     * Set mobileOperator value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \Cashu\TPay\Catalogs\Structs\MobileOperator $mobileOperator
     * @return \Cashu\TPay\Catalogs\Structs\ApiCatalog
     */
    public function setMobileOperator(\Cashu\TPay\Catalogs\Structs\MobileOperator $mobileOperator = null)
    {
        if (is_null($mobileOperator) || (is_array($mobileOperator) && empty($mobileOperator))) {
            unset($this->mobileOperator);
        } else {
            $this->mobileOperator = $mobileOperator;
        }
        return $this;
    }
    /**
     * Get products value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \Cashu\TPay\Catalogs\Arrays\ArrayOfproduct|null
     */
    public function getProducts()
    {
        return isset($this->products) ? $this->products : null;
    }
    /**
     * Set products value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \Cashu\TPay\Catalogs\Arrays\ArrayOfproduct $products
     * @return \Cashu\TPay\Catalogs\Structs\ApiCatalog
     */
    public function setProducts(\Cashu\TPay\Catalogs\Arrays\ArrayOfproduct $products = null)
    {
        if (is_null($products) || (is_array($products) && empty($products))) {
            unset($this->products);
        } else {
            $this->products = $products;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \Cashu\TPay\Catalogs\Structs\ApiCatalog
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}

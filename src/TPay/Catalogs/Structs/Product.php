<?php

namespace Cashu\TPay\Catalogs\Structs;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for product Structs
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:product
 * @subpackage Structs
 */
class Product extends AbstractStructBase
{
    /**
     * The SKU
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $SKU;
    /**
     * The isEnabled
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var bool
     */
    public $isEnabled;
    /**
     * The price
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var float
     */
    public $price;
    /**
     * The pricings
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \Cashu\TPay\Catalogs\Arrays\ArrayOfproductPricing
     */
    public $pricings;
    /**
     * The productId
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $productId;
    /**
     * The productName
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $productName;
    /**
     * Constructor method for product
     * @uses Product::setSKU()
     * @uses Product::setIsEnabled()
     * @uses Product::setPrice()
     * @uses Product::setPricings()
     * @uses Product::setProductId()
     * @uses Product::setProductName()
     * @param string $sKU
     * @param bool $isEnabled
     * @param float $price
     * @param \Cashu\TPay\Catalogs\Arrays\ArrayOfproductPricing $pricings
     * @param int $productId
     * @param string $productName
     */
    public function __construct($sKU = null, $isEnabled = null, $price = null, \Cashu\TPay\Catalogs\Arrays\ArrayOfproductPricing $pricings = null, $productId = null, $productName = null)
    {
        $this
            ->setSKU($sKU)
            ->setIsEnabled($isEnabled)
            ->setPrice($price)
            ->setPricings($pricings)
            ->setProductId($productId)
            ->setProductName($productName);
    }
    /**
     * Get SKU value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSKU()
    {
        return isset($this->SKU) ? $this->SKU : null;
    }
    /**
     * Set SKU value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $sKU
     * @return \Cashu\TPay\Catalogs\Structs\Product
     */
    public function setSKU($sKU = null)
    {
        // validation for constraint: string
        if (!is_null($sKU) && !is_string($sKU)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($sKU)), __LINE__);
        }
        if (is_null($sKU) || (is_array($sKU) && empty($sKU))) {
            unset($this->SKU);
        } else {
            $this->SKU = $sKU;
        }
        return $this;
    }
    /**
     * Get isEnabled value
     * @return bool|null
     */
    public function getIsEnabled()
    {
        return $this->isEnabled;
    }
    /**
     * Set isEnabled value
     * @param bool $isEnabled
     * @return \Cashu\TPay\Catalogs\Structs\Product
     */
    public function setIsEnabled($isEnabled = null)
    {
        $this->isEnabled = $isEnabled;
        return $this;
    }
    /**
     * Get price value
     * @return float|null
     */
    public function getPrice()
    {
        return $this->price;
    }
    /**
     * Set price value
     * @param float $price
     * @return \Cashu\TPay\Catalogs\Structs\Product
     */
    public function setPrice($price = null)
    {
        $this->price = $price;
        return $this;
    }
    /**
     * Get pricings value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \Cashu\TPay\Catalogs\Arrays\ArrayOfproductPricing|null
     */
    public function getPricings()
    {
        return isset($this->pricings) ? $this->pricings : null;
    }
    /**
     * Set pricings value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \Cashu\TPay\Catalogs\Arrays\ArrayOfproductPricing $pricings
     * @return \Cashu\TPay\Catalogs\Structs\Product
     */
    public function setPricings(\Cashu\TPay\Catalogs\Arrays\ArrayOfproductPricing $pricings = null)
    {
        if (is_null($pricings) || (is_array($pricings) && empty($pricings))) {
            unset($this->pricings);
        } else {
            $this->pricings = $pricings;
        }
        return $this;
    }
    /**
     * Get productId value
     * @return int|null
     */
    public function getProductId()
    {
        return $this->productId;
    }
    /**
     * Set productId value
     * @param int $productId
     * @return \Cashu\TPay\Catalogs\Structs\Product
     */
    public function setProductId($productId = null)
    {
        // validation for constraint: int
        if (!is_null($productId) && !is_numeric($productId)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a numeric value, "%s" given', gettype($productId)), __LINE__);
        }
        $this->productId = $productId;
        return $this;
    }
    /**
     * Get productName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getProductName()
    {
        return isset($this->productName) ? $this->productName : null;
    }
    /**
     * Set productName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $productName
     * @return \Cashu\TPay\Catalogs\Structs\Product
     */
    public function setProductName($productName = null)
    {
        // validation for constraint: string
        if (!is_null($productName) && !is_string($productName)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($productName)), __LINE__);
        }
        if (is_null($productName) || (is_array($productName) && empty($productName))) {
            unset($this->productName);
        } else {
            $this->productName = $productName;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \Cashu\TPay\Catalogs\Structs\Product
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}

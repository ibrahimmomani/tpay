<?php

namespace Cashu\TPay\Catalogs\Structs;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for UpdateProduct Structs
 * @subpackage Structs
 */
class UpdateProduct extends AbstractStructBase
{
    /**
     * The productId
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $productId;
    /**
     * The name
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $name;
    /**
     * The sku
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $sku;
    /**
     * The isEnabled
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var bool
     */
    public $isEnabled;
    /**
     * The pricings
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \Cashu\TPay\Catalogs\Arrays\ArrayOfproductPricing
     */
    public $pricings;
    /**
     * The signature
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $signature;
    /**
     * Constructor method for UpdateProduct
     * @uses UpdateProduct::setProductId()
     * @uses UpdateProduct::setName()
     * @uses UpdateProduct::setSku()
     * @uses UpdateProduct::setIsEnabled()
     * @uses UpdateProduct::setPricings()
     * @uses UpdateProduct::setSignature()
     * @param int $productId
     * @param string $name
     * @param string $sku
     * @param bool $isEnabled
     * @param \Cashu\TPay\Catalogs\Arrays\ArrayOfproductPricing $pricings
     * @param string $signature
     */
    public function __construct($productId = null, $name = null, $sku = null, $isEnabled = null, \Cashu\TPay\Catalogs\Arrays\ArrayOfproductPricing $pricings = null, $signature = null)
    {
        $this
            ->setProductId($productId)
            ->setName($name)
            ->setSku($sku)
            ->setIsEnabled($isEnabled)
            ->setPricings($pricings)
            ->setSignature($signature);
    }
    /**
     * Get productId value
     * @return int|null
     */
    public function getProductId()
    {
        return $this->productId;
    }
    /**
     * Set productId value
     * @param int $productId
     * @return \Cashu\TPay\Catalogs\Structs\UpdateProduct
     */
    public function setProductId($productId = null)
    {
        // validation for constraint: int
        if (!is_null($productId) && !is_numeric($productId)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a numeric value, "%s" given', gettype($productId)), __LINE__);
        }
        $this->productId = $productId;
        return $this;
    }
    /**
     * Get name value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getName()
    {
        return isset($this->name) ? $this->name : null;
    }
    /**
     * Set name value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $name
     * @return \Cashu\TPay\Catalogs\Structs\UpdateProduct
     */
    public function setName($name = null)
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($name)), __LINE__);
        }
        if (is_null($name) || (is_array($name) && empty($name))) {
            unset($this->name);
        } else {
            $this->name = $name;
        }
        return $this;
    }
    /**
     * Get sku value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSku()
    {
        return isset($this->sku) ? $this->sku : null;
    }
    /**
     * Set sku value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $sku
     * @return \Cashu\TPay\Catalogs\Structs\UpdateProduct
     */
    public function setSku($sku = null)
    {
        // validation for constraint: string
        if (!is_null($sku) && !is_string($sku)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($sku)), __LINE__);
        }
        if (is_null($sku) || (is_array($sku) && empty($sku))) {
            unset($this->sku);
        } else {
            $this->sku = $sku;
        }
        return $this;
    }
    /**
     * Get isEnabled value
     * @return bool|null
     */
    public function getIsEnabled()
    {
        return $this->isEnabled;
    }
    /**
     * Set isEnabled value
     * @param bool $isEnabled
     * @return \Cashu\TPay\Catalogs\Structs\UpdateProduct
     */
    public function setIsEnabled($isEnabled = null)
    {
        $this->isEnabled = $isEnabled;
        return $this;
    }
    /**
     * Get pricings value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \Cashu\TPay\Catalogs\Arrays\ArrayOfproductPricing|null
     */
    public function getPricings()
    {
        return isset($this->pricings) ? $this->pricings : null;
    }
    /**
     * Set pricings value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \Cashu\TPay\Catalogs\Arrays\ArrayOfproductPricing $pricings
     * @return \Cashu\TPay\Catalogs\Structs\UpdateProduct
     */
    public function setPricings(\Cashu\TPay\Catalogs\Arrays\ArrayOfproductPricing $pricings = null)
    {
        if (is_null($pricings) || (is_array($pricings) && empty($pricings))) {
            unset($this->pricings);
        } else {
            $this->pricings = $pricings;
        }
        return $this;
    }
    /**
     * Get signature value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSignature()
    {
        return isset($this->signature) ? $this->signature : null;
    }
    /**
     * Set signature value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $signature
     * @return \Cashu\TPay\Catalogs\Structs\UpdateProduct
     */
    public function setSignature($signature = null)
    {
        // validation for constraint: string
        if (!is_null($signature) && !is_string($signature)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($signature)), __LINE__);
        }
        if (is_null($signature) || (is_array($signature) && empty($signature))) {
            unset($this->signature);
        } else {
            $this->signature = $signature;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \Cashu\TPay\Catalogs\Structs\UpdateProduct
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}

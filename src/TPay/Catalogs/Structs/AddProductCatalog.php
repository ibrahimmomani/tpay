<?php

namespace Cashu\TPay\Catalogs\Structs;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for AddProductCatalog Structs
 * @subpackage Structs
 */
class AddProductCatalog extends AbstractStructBase
{
    /**
     * The salesChannelId
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $salesChannelId;
    /**
     * The name
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $name;
    /**
     * The isEnabled
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var bool
     */
    public $isEnabled;
    /**
     * The signature
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $signature;
    /**
     * Constructor method for AddProductCatalog
     * @uses AddProductCatalog::setSalesChannelId()
     * @uses AddProductCatalog::setName()
     * @uses AddProductCatalog::setIsEnabled()
     * @uses AddProductCatalog::setSignature()
     * @param int $salesChannelId
     * @param string $name
     * @param bool $isEnabled
     * @param string $signature
     */
    public function __construct($salesChannelId = null, $name = null, $isEnabled = null, $signature = null)
    {
        $this
            ->setSalesChannelId($salesChannelId)
            ->setName($name)
            ->setIsEnabled($isEnabled)
            ->setSignature($signature);
    }
    /**
     * Get salesChannelId value
     * @return int|null
     */
    public function getSalesChannelId()
    {
        return $this->salesChannelId;
    }
    /**
     * Set salesChannelId value
     * @param int $salesChannelId
     * @return \Cashu\TPay\Catalogs\Structs\AddProductCatalog
     */
    public function setSalesChannelId($salesChannelId = null)
    {
        // validation for constraint: int
        if (!is_null($salesChannelId) && !is_numeric($salesChannelId)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a numeric value, "%s" given', gettype($salesChannelId)), __LINE__);
        }
        $this->salesChannelId = $salesChannelId;
        return $this;
    }
    /**
     * Get name value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getName()
    {
        return isset($this->name) ? $this->name : null;
    }
    /**
     * Set name value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $name
     * @return \Cashu\TPay\Catalogs\Structs\AddProductCatalog
     */
    public function setName($name = null)
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($name)), __LINE__);
        }
        if (is_null($name) || (is_array($name) && empty($name))) {
            unset($this->name);
        } else {
            $this->name = $name;
        }
        return $this;
    }
    /**
     * Get isEnabled value
     * @return bool|null
     */
    public function getIsEnabled()
    {
        return $this->isEnabled;
    }
    /**
     * Set isEnabled value
     * @param bool $isEnabled
     * @return \Cashu\TPay\Catalogs\Structs\AddProductCatalog
     */
    public function setIsEnabled($isEnabled = null)
    {
        $this->isEnabled = $isEnabled;
        return $this;
    }
    /**
     * Get signature value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSignature()
    {
        return isset($this->signature) ? $this->signature : null;
    }
    /**
     * Set signature value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $signature
     * @return \Cashu\TPay\Catalogs\Structs\AddProductCatalog
     */
    public function setSignature($signature = null)
    {
        // validation for constraint: string
        if (!is_null($signature) && !is_string($signature)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($signature)), __LINE__);
        }
        if (is_null($signature) || (is_array($signature) && empty($signature))) {
            unset($this->signature);
        } else {
            $this->signature = $signature;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \Cashu\TPay\Catalogs\Structs\AddProductCatalog
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}

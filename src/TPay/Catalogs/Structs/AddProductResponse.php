<?php

namespace Cashu\TPay\Catalogs\Structs;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for AddProductResponse Structs
 * @subpackage Structs
 */
class AddProductResponse extends AbstractStructBase
{
    /**
     * The AddProductResult
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \Cashu\TPay\Catalogs\Structs\Product
     */
    public $AddProductResult;
    /**
     * Constructor method for AddProductResponse
     * @uses AddProductResponse::setAddProductResult()
     * @param \Cashu\TPay\Catalogs\Structs\Product $addProductResult
     */
    public function __construct(\Cashu\TPay\Catalogs\Structs\Product $addProductResult = null)
    {
        $this
            ->setAddProductResult($addProductResult);
    }
    /**
     * Get AddProductResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \Cashu\TPay\Catalogs\Structs\Product|null
     */
    public function getAddProductResult()
    {
        return isset($this->AddProductResult) ? $this->AddProductResult : null;
    }
    /**
     * Set AddProductResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \Cashu\TPay\Catalogs\Structs\Product $addProductResult
     * @return \Cashu\TPay\Catalogs\Structs\AddProductResponse
     */
    public function setAddProductResult(\Cashu\TPay\Catalogs\Structs\Product $addProductResult = null)
    {
        if (is_null($addProductResult) || (is_array($addProductResult) && empty($addProductResult))) {
            unset($this->AddProductResult);
        } else {
            $this->AddProductResult = $addProductResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \Cashu\TPay\Catalogs\Structs\AddProductResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}

<?php

namespace Cashu\TPay\Catalogs\Structs;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetProductsResponse Structs
 * @subpackage Structs
 */
class GetProductsResponse extends AbstractStructBase
{
    /**
     * The GetProductsResult
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \Cashu\TPay\Catalogs\Arrays\ArrayOfproduct
     */
    public $GetProductsResult;
    /**
     * Constructor method for GetProductsResponse
     * @uses GetProductsResponse::setGetProductsResult()
     * @param \Cashu\TPay\Catalogs\Arrays\ArrayOfproduct $getProductsResult
     */
    public function __construct(\Cashu\TPay\Catalogs\Arrays\ArrayOfproduct $getProductsResult = null)
    {
        $this
            ->setGetProductsResult($getProductsResult);
    }
    /**
     * Get GetProductsResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \Cashu\TPay\Catalogs\Arrays\ArrayOfproduct|null
     */
    public function getGetProductsResult()
    {
        return isset($this->GetProductsResult) ? $this->GetProductsResult : null;
    }
    /**
     * Set GetProductsResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \Cashu\TPay\Catalogs\Arrays\ArrayOfproduct $getProductsResult
     * @return \Cashu\TPay\Catalogs\Structs\GetProductsResponse
     */
    public function setGetProductsResult(\Cashu\TPay\Catalogs\Arrays\ArrayOfproduct $getProductsResult = null)
    {
        if (is_null($getProductsResult) || (is_array($getProductsResult) && empty($getProductsResult))) {
            unset($this->GetProductsResult);
        } else {
            $this->GetProductsResult = $getProductsResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \Cashu\TPay\Catalogs\Structs\GetProductsResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}

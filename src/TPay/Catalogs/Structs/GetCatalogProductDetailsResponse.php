<?php

namespace Cashu\TPay\Catalogs\Structs;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetCatalogProductDetailsResponse Structs
 * @subpackage Structs
 */
class GetCatalogProductDetailsResponse extends AbstractStructBase
{
    /**
     * The GetCatalogProductDetailsResult
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \Cashu\TPay\Catalogs\Structs\ProductDetails
     */
    public $GetCatalogProductDetailsResult;
    /**
     * Constructor method for GetCatalogProductDetailsResponse
     * @uses GetCatalogProductDetailsResponse::setGetCatalogProductDetailsResult()
     * @param \Cashu\TPay\Catalogs\Structs\ProductDetails $getCatalogProductDetailsResult
     */
    public function __construct(\Cashu\TPay\Catalogs\Structs\ProductDetails $getCatalogProductDetailsResult = null)
    {
        $this
            ->setGetCatalogProductDetailsResult($getCatalogProductDetailsResult);
    }
    /**
     * Get GetCatalogProductDetailsResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \Cashu\TPay\Catalogs\Structs\ProductDetails|null
     */
    public function getGetCatalogProductDetailsResult()
    {
        return isset($this->GetCatalogProductDetailsResult) ? $this->GetCatalogProductDetailsResult : null;
    }
    /**
     * Set GetCatalogProductDetailsResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \Cashu\TPay\Catalogs\Structs\ProductDetails $getCatalogProductDetailsResult
     * @return \Cashu\TPay\Catalogs\Structs\GetCatalogProductDetailsResponse
     */
    public function setGetCatalogProductDetailsResult(\Cashu\TPay\Catalogs\Structs\ProductDetails $getCatalogProductDetailsResult = null)
    {
        if (is_null($getCatalogProductDetailsResult) || (is_array($getCatalogProductDetailsResult) && empty($getCatalogProductDetailsResult))) {
            unset($this->GetCatalogProductDetailsResult);
        } else {
            $this->GetCatalogProductDetailsResult = $getCatalogProductDetailsResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \Cashu\TPay\Catalogs\Structs\GetCatalogProductDetailsResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}

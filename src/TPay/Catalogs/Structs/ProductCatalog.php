<?php

namespace Cashu\TPay\Catalogs\Structs;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for productCatalog Structs
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:productCatalog
 * @subpackage Structs
 */
class ProductCatalog extends AbstractStructBase
{
    /**
     * The catalogDescription
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $catalogDescription;
    /**
     * The catalogId
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $catalogId;
    /**
     * The catalogName
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $catalogName;
    /**
     * The isEnabled
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var bool
     */
    public $isEnabled;
    /**
     * The salesChannelId
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $salesChannelId;
    /**
     * Constructor method for productCatalog
     * @uses ProductCatalog::setCatalogDescription()
     * @uses ProductCatalog::setCatalogId()
     * @uses ProductCatalog::setCatalogName()
     * @uses ProductCatalog::setIsEnabled()
     * @uses ProductCatalog::setSalesChannelId()
     * @param string $catalogDescription
     * @param int $catalogId
     * @param string $catalogName
     * @param bool $isEnabled
     * @param int $salesChannelId
     */
    public function __construct($catalogDescription = null, $catalogId = null, $catalogName = null, $isEnabled = null, $salesChannelId = null)
    {
        $this
            ->setCatalogDescription($catalogDescription)
            ->setCatalogId($catalogId)
            ->setCatalogName($catalogName)
            ->setIsEnabled($isEnabled)
            ->setSalesChannelId($salesChannelId);
    }
    /**
     * Get catalogDescription value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCatalogDescription()
    {
        return isset($this->catalogDescription) ? $this->catalogDescription : null;
    }
    /**
     * Set catalogDescription value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $catalogDescription
     * @return \Cashu\TPay\Catalogs\Structs\ProductCatalog
     */
    public function setCatalogDescription($catalogDescription = null)
    {
        // validation for constraint: string
        if (!is_null($catalogDescription) && !is_string($catalogDescription)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($catalogDescription)), __LINE__);
        }
        if (is_null($catalogDescription) || (is_array($catalogDescription) && empty($catalogDescription))) {
            unset($this->catalogDescription);
        } else {
            $this->catalogDescription = $catalogDescription;
        }
        return $this;
    }
    /**
     * Get catalogId value
     * @return int|null
     */
    public function getCatalogId()
    {
        return $this->catalogId;
    }
    /**
     * Set catalogId value
     * @param int $catalogId
     * @return \Cashu\TPay\Catalogs\Structs\ProductCatalog
     */
    public function setCatalogId($catalogId = null)
    {
        // validation for constraint: int
        if (!is_null($catalogId) && !is_numeric($catalogId)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a numeric value, "%s" given', gettype($catalogId)), __LINE__);
        }
        $this->catalogId = $catalogId;
        return $this;
    }
    /**
     * Get catalogName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCatalogName()
    {
        return isset($this->catalogName) ? $this->catalogName : null;
    }
    /**
     * Set catalogName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $catalogName
     * @return \Cashu\TPay\Catalogs\Structs\ProductCatalog
     */
    public function setCatalogName($catalogName = null)
    {
        // validation for constraint: string
        if (!is_null($catalogName) && !is_string($catalogName)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($catalogName)), __LINE__);
        }
        if (is_null($catalogName) || (is_array($catalogName) && empty($catalogName))) {
            unset($this->catalogName);
        } else {
            $this->catalogName = $catalogName;
        }
        return $this;
    }
    /**
     * Get isEnabled value
     * @return bool|null
     */
    public function getIsEnabled()
    {
        return $this->isEnabled;
    }
    /**
     * Set isEnabled value
     * @param bool $isEnabled
     * @return \Cashu\TPay\Catalogs\Structs\ProductCatalog
     */
    public function setIsEnabled($isEnabled = null)
    {
        $this->isEnabled = $isEnabled;
        return $this;
    }
    /**
     * Get salesChannelId value
     * @return int|null
     */
    public function getSalesChannelId()
    {
        return $this->salesChannelId;
    }
    /**
     * Set salesChannelId value
     * @param int $salesChannelId
     * @return \Cashu\TPay\Catalogs\Structs\ProductCatalog
     */
    public function setSalesChannelId($salesChannelId = null)
    {
        // validation for constraint: int
        if (!is_null($salesChannelId) && !is_numeric($salesChannelId)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a numeric value, "%s" given', gettype($salesChannelId)), __LINE__);
        }
        $this->salesChannelId = $salesChannelId;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \Cashu\TPay\Catalogs\Structs\ProductCatalog
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}

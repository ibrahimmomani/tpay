<?php

namespace Cashu\TPay\Catalogs\Structs;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetCatalogDetailsResponse Structs
 * @subpackage Structs
 */
class GetCatalogDetailsResponse extends AbstractStructBase
{
    /**
     * The GetCatalogDetailsResult
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \Cashu\TPay\Catalogs\Arrays\ArrayOfApiCatalog
     */
    public $GetCatalogDetailsResult;
    /**
     * Constructor method for GetCatalogDetailsResponse
     * @uses GetCatalogDetailsResponse::setGetCatalogDetailsResult()
     * @param \Cashu\TPay\Catalogs\Arrays\ArrayOfApiCatalog $getCatalogDetailsResult
     */
    public function __construct(\Cashu\TPay\Catalogs\Arrays\ArrayOfApiCatalog $getCatalogDetailsResult = null)
    {
        $this
            ->setGetCatalogDetailsResult($getCatalogDetailsResult);
    }
    /**
     * Get GetCatalogDetailsResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \Cashu\TPay\Catalogs\Arrays\ArrayOfApiCatalog|null
     */
    public function getGetCatalogDetailsResult()
    {
        return isset($this->GetCatalogDetailsResult) ? $this->GetCatalogDetailsResult : null;
    }
    /**
     * Set GetCatalogDetailsResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \Cashu\TPay\Catalogs\Arrays\ArrayOfApiCatalog $getCatalogDetailsResult
     * @return \Cashu\TPay\Catalogs\Structs\GetCatalogDetailsResponse
     */
    public function setGetCatalogDetailsResult(\Cashu\TPay\Catalogs\Arrays\ArrayOfApiCatalog $getCatalogDetailsResult = null)
    {
        if (is_null($getCatalogDetailsResult) || (is_array($getCatalogDetailsResult) && empty($getCatalogDetailsResult))) {
            unset($this->GetCatalogDetailsResult);
        } else {
            $this->GetCatalogDetailsResult = $getCatalogDetailsResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \Cashu\TPay\Catalogs\Structs\GetCatalogDetailsResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}

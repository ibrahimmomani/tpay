<?php

namespace Cashu\TPay\Catalogs\Structs;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for AddProductCatalogResponse Structs
 * @subpackage Structs
 */
class AddProductCatalogResponse extends AbstractStructBase
{
    /**
     * The AddProductCatalogResult
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \Cashu\TPay\Catalogs\Structs\ProductCatalog
     */
    public $AddProductCatalogResult;
    /**
     * Constructor method for AddProductCatalogResponse
     * @uses AddProductCatalogResponse::setAddProductCatalogResult()
     * @param \Cashu\TPay\Catalogs\Structs\ProductCatalog $addProductCatalogResult
     */
    public function __construct(\Cashu\TPay\Catalogs\Structs\ProductCatalog $addProductCatalogResult = null)
    {
        $this
            ->setAddProductCatalogResult($addProductCatalogResult);
    }
    /**
     * Get AddProductCatalogResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \Cashu\TPay\Catalogs\Structs\ProductCatalog|null
     */
    public function getAddProductCatalogResult()
    {
        return isset($this->AddProductCatalogResult) ? $this->AddProductCatalogResult : null;
    }
    /**
     * Set AddProductCatalogResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \Cashu\TPay\Catalogs\Structs\ProductCatalog $addProductCatalogResult
     * @return \Cashu\TPay\Catalogs\Structs\AddProductCatalogResponse
     */
    public function setAddProductCatalogResult(\Cashu\TPay\Catalogs\Structs\ProductCatalog $addProductCatalogResult = null)
    {
        if (is_null($addProductCatalogResult) || (is_array($addProductCatalogResult) && empty($addProductCatalogResult))) {
            unset($this->AddProductCatalogResult);
        } else {
            $this->AddProductCatalogResult = $addProductCatalogResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \Cashu\TPay\Catalogs\Structs\AddProductCatalogResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}

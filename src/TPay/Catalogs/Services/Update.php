<?php

namespace Cashu\TPay\Catalogs\Services;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Update Services
 * @subpackage Services
 */
class Update extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named UpdateProductCatalog
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Cashu\TPay\Catalogs\Structs\UpdateProductCatalog $parameters
     * @return \Cashu\TPay\Catalogs\Structs\UpdateProductCatalogResponse|bool
     */
    public function UpdateProductCatalog(\Cashu\TPay\Catalogs\Structs\UpdateProductCatalog $parameters)
    {
        try {
            $this->setResult(self::getSoapClient()->UpdateProductCatalog($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named UpdateProduct
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Cashu\TPay\Catalogs\Structs\UpdateProduct $parameters
     * @return \Cashu\TPay\Catalogs\Structs\UpdateProductResponse|bool
     */
    public function UpdateProduct(\Cashu\TPay\Catalogs\Structs\UpdateProduct $parameters)
    {
        try {
            $this->setResult(self::getSoapClient()->UpdateProduct($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \Cashu\TPay\Catalogs\Structs\UpdateProductCatalogResponse|\Cashu\TPay\Catalogs\Structs\UpdateProductResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}

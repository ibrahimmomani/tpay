<?php

namespace Cashu\TPay\Catalogs\Services;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Add Services
 * @subpackage Services
 */
class Add extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named AddProductCatalog
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Cashu\TPay\Catalogs\Structs\AddProductCatalog $parameters
     * @return \Cashu\TPay\Catalogs\Structs\AddProductCatalogResponse|bool
     */
    public function AddProductCatalog(\Cashu\TPay\Catalogs\Structs\AddProductCatalog $parameters)
    {
        try {
            $this->setResult(self::getSoapClient()->AddProductCatalog($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named AddProduct
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Cashu\TPay\Catalogs\Structs\AddProduct $parameters
     * @return \Cashu\TPay\Catalogs\Structs\AddProductResponse|bool
     */
    public function AddProduct(\Cashu\TPay\Catalogs\Structs\AddProduct $parameters)
    {
        try {
            $this->setResult(self::getSoapClient()->AddProduct($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \Cashu\TPay\Catalogs\Structs\AddProductCatalogResponse|\Cashu\TPay\Catalogs\Structs\AddProductResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}

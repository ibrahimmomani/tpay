<?php

namespace Cashu\TPay\Catalogs\Services;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Get Services
 * @subpackage Services
 */
class Get extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named GetMobileOperators
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Cashu\TPay\Catalogs\Structs\GetMobileOperators $parameters
     * @return \Cashu\TPay\Catalogs\Structs\GetMobileOperatorsResponse|bool
     */
    public function GetMobileOperators(\Cashu\TPay\Catalogs\Structs\GetMobileOperators $parameters)
    {
        try {
            $this->setResult(self::getSoapClient()->GetMobileOperators($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetSalesChannels
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Cashu\TPay\Catalogs\Structs\GetSalesChannels $parameters
     * @return \Cashu\TPay\Catalogs\Structs\GetSalesChannelsResponse|bool
     */
    public function GetSalesChannels(\Cashu\TPay\Catalogs\Structs\GetSalesChannels $parameters)
    {
        try {
            $this->setResult(self::getSoapClient()->GetSalesChannels($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetProductCatalogs
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Cashu\TPay\Catalogs\Structs\GetProductCatalogs $parameters
     * @return \Cashu\TPay\Catalogs\Structs\GetProductCatalogsResponse|bool
     */
    public function GetProductCatalogs(\Cashu\TPay\Catalogs\Structs\GetProductCatalogs $parameters)
    {
        try {
            $this->setResult(self::getSoapClient()->GetProductCatalogs($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetProducts
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Cashu\TPay\Catalogs\Structs\GetProducts $parameters
     * @return \Cashu\TPay\Catalogs\Structs\GetProductsResponse|bool
     */
    public function GetProducts(\Cashu\TPay\Catalogs\Structs\GetProducts $parameters)
    {
        try {
            $this->setResult(self::getSoapClient()->GetProducts($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetCatalogDetails
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Cashu\TPay\Catalogs\Structs\GetCatalogDetails $parameters
     * @return \Cashu\TPay\Catalogs\Structs\GetCatalogDetailsResponse|bool
     */
    public function GetCatalogDetails(\Cashu\TPay\Catalogs\Structs\GetCatalogDetails $parameters)
    {
        try {
            $this->setResult(self::getSoapClient()->GetCatalogDetails($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetCatalogProductDetails
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Cashu\TPay\Catalogs\Structs\GetCatalogProductDetails $parameters
     * @return \Cashu\TPay\Catalogs\Structs\GetCatalogProductDetailsResponse|bool
     */
    public function GetCatalogProductDetails(\Cashu\TPay\Catalogs\Structs\GetCatalogProductDetails $parameters)
    {
        try {
            $this->setResult(self::getSoapClient()->GetCatalogProductDetails($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetCatalogProducts
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Cashu\TPay\Catalogs\Structs\GetCatalogProducts $parameters
     * @return \Cashu\TPay\Catalogs\Structs\GetCatalogProductsResponse|bool
     */
    public function GetCatalogProducts(\Cashu\TPay\Catalogs\Structs\GetCatalogProducts $parameters)
    {
        try {
            $this->setResult(self::getSoapClient()->GetCatalogProducts($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \Cashu\TPay\Catalogs\Structs\GetCatalogDetailsResponse|\Cashu\TPay\Catalogs\Structs\GetCatalogProductDetailsResponse|\Cashu\TPay\Catalogs\Structs\GetCatalogProductsResponse|\Cashu\TPay\Catalogs\Structs\GetMobileOperatorsResponse|\Cashu\TPay\Catalogs\Structs\GetProductCatalogsResponse|\Cashu\TPay\Catalogs\Structs\GetProductsResponse|\Cashu\TPay\Catalogs\Structs\GetSalesChannelsResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}

<?php

namespace Cashu\TPay\Catalogs\Services;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Delete Services
 * @subpackage Services
 */
class Delete extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named DeleteProduct
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Cashu\TPay\Catalogs\Structs\DeleteProduct $parameters
     * @return \Cashu\TPay\Catalogs\Structs\DeleteProductResponse|bool
     */
    public function DeleteProduct(\Cashu\TPay\Catalogs\Structs\DeleteProduct $parameters)
    {
        try {
            $this->setResult(self::getSoapClient()->DeleteProduct($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named DeleteProductCatalog
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Cashu\TPay\Catalogs\Structs\DeleteProductCatalog $parameters
     * @return \Cashu\TPay\Catalogs\Structs\DeleteProductCatalogResponse|bool
     */
    public function DeleteProductCatalog(\Cashu\TPay\Catalogs\Structs\DeleteProductCatalog $parameters)
    {
        try {
            $this->setResult(self::getSoapClient()->DeleteProductCatalog($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \Cashu\TPay\Catalogs\Structs\DeleteProductCatalogResponse|\Cashu\TPay\Catalogs\Structs\DeleteProductResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}

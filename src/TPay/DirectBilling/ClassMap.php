<?php

namespace Cashu\TPay\DirectBilling;

/**
 * Class which returns the class map definition
 * @package
 */
class ClassMap
{
    /**
     * Returns the mapping between the WSDL Structs and generated Structs' classes
     * This array is sent to the \SoapClient when calling the WS
     * @return string[]
     */
    final public static function get()
    {
        return array(
            'InitializeDirectPaymentTransaction' => '\\Cashu\\TPay\\DirectBilling\\Structs\\InitializeDirectPaymentTransaction',
            'directPaymentRequest' => '\\Cashu\\TPay\\DirectBilling\\Structs\\DirectPaymentRequest',
            'InitializeDirectPaymentTransactionResponse' => '\\Cashu\\TPay\\DirectBilling\\Structs\\InitializeDirectPaymentTransactionResponse',
            'directPaymentResponse' => '\\Cashu\\TPay\\DirectBilling\\Structs\\DirectPaymentResponse',
            'directPaymentFault' => '\\Cashu\\TPay\\DirectBilling\\Structs\\DirectPaymentFault',
            'ConfirmDirectPaymentTransaction' => '\\Cashu\\TPay\\DirectBilling\\Structs\\ConfirmDirectPaymentTransaction',
            'ConfirmDirectPaymentTransactionResponse' => '\\Cashu\\TPay\\DirectBilling\\Structs\\ConfirmDirectPaymentTransactionResponse',
            'ResendVerificationPin' => '\\Cashu\\TPay\\DirectBilling\\Structs\\ResendVerificationPin',
            'ResendVerificationPinResponse' => '\\Cashu\\TPay\\DirectBilling\\Structs\\ResendVerificationPinResponse',
            'tpayResponse' => '\\Cashu\\TPay\\DirectBilling\\Structs\\TpayResponse',
            'ResendVerificationPinThroughIVR' => '\\Cashu\\TPay\\DirectBilling\\Structs\\ResendVerificationPinThroughIVR',
            'ResendVerificationPinThroughIVRResponse' => '\\Cashu\\TPay\\DirectBilling\\Structs\\ResendVerificationPinThroughIVRResponse',

        );
    }
}

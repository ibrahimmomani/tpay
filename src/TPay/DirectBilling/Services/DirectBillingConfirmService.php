<?php

namespace Cashu\TPay\DirectBilling\Services;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for DirectBillingConfirmService Services
 * @subpackage Services
 */
class DirectBillingConfirmService extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named ConfirmDirectPaymentTransaction
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Cashu\TPay\DirectBilling\Structs\ConfirmDirectPaymentTransaction $parameters
     * @return \Cashu\TPay\DirectBilling\Structs\ConfirmDirectPaymentTransactionResponse|bool
     */
    public function ConfirmDirectPaymentTransaction(\Cashu\TPay\DirectBilling\Structs\ConfirmDirectPaymentTransaction $parameters)
    {
        try {
            $this->setResult(self::getSoapClient()->ConfirmDirectPaymentTransaction($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \Cashu\TPay\DirectBilling\Structs\ConfirmDirectPaymentTransactionResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}

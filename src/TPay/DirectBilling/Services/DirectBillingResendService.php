<?php

namespace Cashu\TPay\DirectBilling\Services;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Resend Services
 * @subpackage Services
 */
class DirectBillingResendService extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named ResendVerificationPin
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Cashu\TPay\DirectBilling\Structs\ResendVerificationPin $parameters
     * @return \Cashu\TPay\DirectBilling\Structs\ResendVerificationPinResponse|bool
     */
    public function ResendVerificationPin(\Cashu\TPay\DirectBilling\Structs\ResendVerificationPin $parameters)
    {
        try {
            $this->setResult(self::getSoapClient()->ResendVerificationPin($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named ResendVerificationPinThroughIVR
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Cashu\TPay\DirectBilling\Structs\ResendVerificationPinThroughIVR $parameters
     * @return \Cashu\TPay\DirectBilling\Structs\ResendVerificationPinThroughIVRResponse|bool
     */
    public function ResendVerificationPinThroughIVR(\Cashu\TPay\DirectBilling\Structs\ResendVerificationPinThroughIVR $parameters)
    {
        try {
            $this->setResult(self::getSoapClient()->ResendVerificationPinThroughIVR($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \Cashu\TPay\DirectBilling\Structs\ResendVerificationPinResponse|\Cashu\TPay\DirectBilling\Structs\ResendVerificationPinThroughIVRResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}

<?php

namespace Cashu\TPay\DirectBilling\Enums;

/**
 * This class stands for directPaymentStatus Enums
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:directPaymentStatus
 * @subpackage Enumerations
 */
class DirectPaymentStatus
{
    /**
     * Constant for value 'PaymentCompletedSuccessfully'
     * @return string 'PaymentCompletedSuccessfully'
     */
    const VALUE_PAYMENT_COMPLETED_SUCCESSFULLY = 'PaymentCompletedSuccessfully';
    /**
     * Constant for value 'FailedToSendVerificationPin'
     * @return string 'FailedToSendVerificationPin'
     */
    const VALUE_FAILED_TO_SEND_VERIFICATION_PIN = 'FailedToSendVerificationPin';
    /**
     * Constant for value 'TransactionCancelledByUser'
     * @return string 'TransactionCancelledByUser'
     */
    const VALUE_TRANSACTION_CANCELLED_BY_USER = 'TransactionCancelledByUser';
    /**
     * Constant for value 'FailedToContactMobileNetworkOperator'
     * @return string 'FailedToContactMobileNetworkOperator'
     */
    const VALUE_FAILED_TO_CONTACT_MOBILE_NETWORK_OPERATOR = 'FailedToContactMobileNetworkOperator';
    /**
     * Constant for value 'NotEnoughCredit'
     * @return string 'NotEnoughCredit'
     */
    const VALUE_NOT_ENOUGH_CREDIT = 'NotEnoughCredit';
    /**
     * Constant for value 'LimitExceeded'
     * @return string 'LimitExceeded'
     */
    const VALUE_LIMIT_EXCEEDED = 'LimitExceeded';
    /**
     * Constant for value 'OperationRejectedByMobileNetworkOperator'
     * @return string 'OperationRejectedByMobileNetworkOperator'
     */
    const VALUE_OPERATION_REJECTED_BY_MOBILE_NETWORK_OPERATOR = 'OperationRejectedByMobileNetworkOperator';
    /**
     * Constant for value 'OperationRejectedByTpay'
     * @return string 'OperationRejectedByTpay'
     */
    const VALUE_OPERATION_REJECTED_BY_TPAY = 'OperationRejectedByTpay';
    /**
     * Constant for value 'InactiveLine'
     * @return string 'InactiveLine'
     */
    const VALUE_INACTIVE_LINE = 'InactiveLine';
    /**
     * Constant for value 'UnspecifiedError'
     * @return string 'UnspecifiedError'
     */
    const VALUE_UNSPECIFIED_ERROR = 'UnspecifiedError';
    /**
     * Constant for value 'VerificationCodeSent'
     * @return string 'VerificationCodeSent'
     */
    const VALUE_VERIFICATION_CODE_SENT = 'VerificationCodeSent';
    /**
     * Constant for value 'InvalidPIN'
     * @return string 'InvalidPIN'
     */
    const VALUE_INVALID_PIN = 'InvalidPIN';
    /**
     * Constant for value 'Created'
     * @return string 'Created'
     */
    const VALUE_CREATED = 'Created';
    /**
     * Constant for value 'SubscriberDoesNotExist'
     * @return string 'SubscriberDoesNotExist'
     */
    const VALUE_SUBSCRIBER_DOES_NOT_EXIST = 'SubscriberDoesNotExist';
    /**
     * Constant for value 'CorporateNotEligible'
     * @return string 'CorporateNotEligible'
     */
    const VALUE_CORPORATE_NOT_ELIGIBLE = 'CorporateNotEligible';
    /**
     * Constant for value 'MobileNetworkOperatorChargingError'
     * @return string 'MobileNetworkOperatorChargingError'
     */
    const VALUE_MOBILE_NETWORK_OPERATOR_CHARGING_ERROR = 'MobileNetworkOperatorChargingError';
    /**
     * Constant for value 'ExpiredPinCode'
     * @return string 'ExpiredPinCode'
     */
    const VALUE_EXPIRED_PIN_CODE = 'ExpiredPinCode';
    /**
     * Constant for value 'Reverted'
     * @return string 'Reverted'
     */
    const VALUE_REVERTED = 'Reverted';
    /**
     * Constant for value 'NotSupportedPricePoint'
     * @return string 'NotSupportedPricePoint'
     */
    const VALUE_NOT_SUPPORTED_PRICE_POINT = 'NotSupportedPricePoint';
    /**
     * Constant for value 'TimeOut'
     * @return string 'TimeOut'
     */
    const VALUE_TIME_OUT = 'TimeOut';
    /**
     * Constant for value 'TransactionUnderProcessing'
     * @return string 'TransactionUnderProcessing'
     */
    const VALUE_TRANSACTION_UNDER_PROCESSING = 'TransactionUnderProcessing';
    /**
     * Constant for value 'Error'
     * @return string 'Error'
     */
    const VALUE_ERROR = 'Error';
    /**
     * Constant for value 'Undefined'
     * @return string 'Undefined'
     */
    const VALUE_UNDEFINED = 'Undefined';
    /**
     * Constant for value 'SucceededAndFailedToContactMerchant'
     * @return string 'SucceededAndFailedToContactMerchant'
     */
    const VALUE_SUCCEEDED_AND_FAILED_TO_CONTACT_MERCHANT = 'SucceededAndFailedToContactMerchant';
    /**
     * Constant for value 'SucceededAndRetryAttemptReached'
     * @return string 'SucceededAndRetryAttemptReached'
     */
    const VALUE_SUCCEEDED_AND_RETRY_ATTEMPT_REACHED = 'SucceededAndRetryAttemptReached';
    /**
     * Constant for value 'PendingTransaction'
     * @return string 'PendingTransaction'
     */
    const VALUE_PENDING_TRANSACTION = 'PendingTransaction';
    /**
     * Return true if value is allowed
     * @uses self::getValidValues()
     * @param mixed $value value
     * @return bool true|false
     */
    public static function valueIsValid($value)
    {
        return ($value === null) || in_array($value, self::getValidValues(), true);
    }
    /**
     * Return allowed values
     * @uses self::VALUE_PAYMENT_COMPLETED_SUCCESSFULLY
     * @uses self::VALUE_FAILED_TO_SEND_VERIFICATION_PIN
     * @uses self::VALUE_TRANSACTION_CANCELLED_BY_USER
     * @uses self::VALUE_FAILED_TO_CONTACT_MOBILE_NETWORK_OPERATOR
     * @uses self::VALUE_NOT_ENOUGH_CREDIT
     * @uses self::VALUE_LIMIT_EXCEEDED
     * @uses self::VALUE_OPERATION_REJECTED_BY_MOBILE_NETWORK_OPERATOR
     * @uses self::VALUE_OPERATION_REJECTED_BY_TPAY
     * @uses self::VALUE_INACTIVE_LINE
     * @uses self::VALUE_UNSPECIFIED_ERROR
     * @uses self::VALUE_VERIFICATION_CODE_SENT
     * @uses self::VALUE_INVALID_PIN
     * @uses self::VALUE_CREATED
     * @uses self::VALUE_SUBSCRIBER_DOES_NOT_EXIST
     * @uses self::VALUE_CORPORATE_NOT_ELIGIBLE
     * @uses self::VALUE_MOBILE_NETWORK_OPERATOR_CHARGING_ERROR
     * @uses self::VALUE_EXPIRED_PIN_CODE
     * @uses self::VALUE_REVERTED
     * @uses self::VALUE_NOT_SUPPORTED_PRICE_POINT
     * @uses self::VALUE_TIME_OUT
     * @uses self::VALUE_TRANSACTION_UNDER_PROCESSING
     * @uses self::VALUE_ERROR
     * @uses self::VALUE_UNDEFINED
     * @uses self::VALUE_SUCCEEDED_AND_FAILED_TO_CONTACT_MERCHANT
     * @uses self::VALUE_SUCCEEDED_AND_RETRY_ATTEMPT_REACHED
     * @uses self::VALUE_PENDING_TRANSACTION
     * @return string[]
     */
    public static function getValidValues()
    {
        return array(
            self::VALUE_PAYMENT_COMPLETED_SUCCESSFULLY,
            self::VALUE_FAILED_TO_SEND_VERIFICATION_PIN,
            self::VALUE_TRANSACTION_CANCELLED_BY_USER,
            self::VALUE_FAILED_TO_CONTACT_MOBILE_NETWORK_OPERATOR,
            self::VALUE_NOT_ENOUGH_CREDIT,
            self::VALUE_LIMIT_EXCEEDED,
            self::VALUE_OPERATION_REJECTED_BY_MOBILE_NETWORK_OPERATOR,
            self::VALUE_OPERATION_REJECTED_BY_TPAY,
            self::VALUE_INACTIVE_LINE,
            self::VALUE_UNSPECIFIED_ERROR,
            self::VALUE_VERIFICATION_CODE_SENT,
            self::VALUE_INVALID_PIN,
            self::VALUE_CREATED,
            self::VALUE_SUBSCRIBER_DOES_NOT_EXIST,
            self::VALUE_CORPORATE_NOT_ELIGIBLE,
            self::VALUE_MOBILE_NETWORK_OPERATOR_CHARGING_ERROR,
            self::VALUE_EXPIRED_PIN_CODE,
            self::VALUE_REVERTED,
            self::VALUE_NOT_SUPPORTED_PRICE_POINT,
            self::VALUE_TIME_OUT,
            self::VALUE_TRANSACTION_UNDER_PROCESSING,
            self::VALUE_ERROR,
            self::VALUE_UNDEFINED,
            self::VALUE_SUCCEEDED_AND_FAILED_TO_CONTACT_MERCHANT,
            self::VALUE_SUCCEEDED_AND_RETRY_ATTEMPT_REACHED,
            self::VALUE_PENDING_TRANSACTION,
        );
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}

<?php

namespace Cashu\TPay\DirectBilling\Enums;

/**
 * This class stands for merchantLanguage Enums
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:merchantLanguage
 * @subpackage Enumerations
 */
class MerchantLanguage
{
    /**
     * Constant for value 'Auto'
     * @return string 'Auto'
     */
    const VALUE_AUTO = 'Auto';
    /**
     * Constant for value 'English'
     * @return string 'English'
     */
    const VALUE_ENGLISH = 'English';
    /**
     * Constant for value 'Arabic'
     * @return string 'Arabic'
     */
    const VALUE_ARABIC = 'Arabic';
    /**
     * Constant for value 'French'
     * @return string 'French'
     */
    const VALUE_FRENCH = 'French';
    /**
     * Return true if value is allowed
     * @uses self::getValidValues()
     * @param mixed $value value
     * @return bool true|false
     */
    public static function valueIsValid($value)
    {
        return ($value === null) || in_array($value, self::getValidValues(), true);
    }
    /**
     * Return allowed values
     * @uses self::VALUE_AUTO
     * @uses self::VALUE_ENGLISH
     * @uses self::VALUE_ARABIC
     * @uses self::VALUE_FRENCH
     * @return string[]
     */
    public static function getValidValues()
    {
        return array(
            self::VALUE_AUTO,
            self::VALUE_ENGLISH,
            self::VALUE_ARABIC,
            self::VALUE_FRENCH,
        );
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}

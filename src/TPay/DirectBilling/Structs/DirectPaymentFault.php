<?php

namespace Cashu\TPay\DirectBilling\Structs;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for directPaymentFault Structs
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:directPaymentFault
 * @subpackage Structs
 */
class DirectPaymentFault extends AbstractStructBase
{
    /**
     * The operationStatusCode
     * @var string
     */
    public $operationStatusCode;
    /**
     * The transactionId
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $transactionId;
    /**
     * Constructor method for directPaymentFault
     * @uses DirectPaymentFault::setOperationStatusCode()
     * @uses DirectPaymentFault::setTransactionId()
     * @param string $operationStatusCode
     * @param string $transactionId
     */
    public function __construct($operationStatusCode = null, $transactionId = null)
    {
        $this
            ->setOperationStatusCode($operationStatusCode)
            ->setTransactionId($transactionId);
    }
    /**
     * Get operationStatusCode value
     * @return string|null
     */
    public function getOperationStatusCode()
    {
        return $this->operationStatusCode;
    }
    /**
     * Set operationStatusCode value
     * @uses \Cashu\TPay\DirectBilling\Enums\DirectPaymentStatus::valueIsValid()
     * @uses \Cashu\TPay\DirectBilling\Enums\DirectPaymentStatus::getValidValues()
     * @throws \InvalidArgumentException
     * @param string $operationStatusCode
     * @return \Cashu\TPay\DirectBilling\Structs\DirectPaymentFault
     */
    public function setOperationStatusCode($operationStatusCode = null)
    {
        // validation for constraint: enumeration
        if (!\Cashu\TPay\DirectBilling\Enums\DirectPaymentStatus::valueIsValid($operationStatusCode)) {
            throw new \InvalidArgumentException(sprintf('Value "%s" is invalid, please use one of: %s', $operationStatusCode, implode(', ', \Cashu\TPay\DirectBilling\Enums\DirectPaymentStatus::getValidValues())), __LINE__);
        }
        $this->operationStatusCode = $operationStatusCode;
        return $this;
    }
    /**
     * Get transactionId value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getTransactionId()
    {
        return isset($this->transactionId) ? $this->transactionId : null;
    }
    /**
     * Set transactionId value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $transactionId
     * @return \Cashu\TPay\DirectBilling\Structs\DirectPaymentFault
     */
    public function setTransactionId($transactionId = null)
    {
        // validation for constraint: string
        if (!is_null($transactionId) && !is_string($transactionId)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($transactionId)), __LINE__);
        }
        if (is_null($transactionId) || (is_array($transactionId) && empty($transactionId))) {
            unset($this->transactionId);
        } else {
            $this->transactionId = $transactionId;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \Cashu\TPay\DirectBilling\Structs\DirectPaymentFault
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}

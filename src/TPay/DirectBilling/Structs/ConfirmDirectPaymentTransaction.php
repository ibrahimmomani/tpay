<?php

namespace Cashu\TPay\DirectBilling\Structs;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ConfirmDirectPaymentTransaction Structs
 * @subpackage Structs
 */
class ConfirmDirectPaymentTransaction extends AbstractStructBase
{
    /**
     * The transactionId
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $transactionId;
    /**
     * The pinCode
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $pinCode;
    /**
     * The signature
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $signature;
    /**
     * Constructor method for ConfirmDirectPaymentTransaction
     * @uses ConfirmDirectPaymentTransaction::setTransactionId()
     * @uses ConfirmDirectPaymentTransaction::setPinCode()
     * @uses ConfirmDirectPaymentTransaction::setSignature()
     * @param string $transactionId
     * @param string $pinCode
     * @param string $signature
     */
    public function __construct($transactionId = null, $pinCode = null, $signature = null)
    {
        $this
            ->setTransactionId($transactionId)
            ->setPinCode($pinCode)
            ->setSignature($signature);
    }
    /**
     * Get transactionId value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getTransactionId()
    {
        return isset($this->transactionId) ? $this->transactionId : null;
    }
    /**
     * Set transactionId value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $transactionId
     * @return \Cashu\TPay\DirectBilling\Structs\ConfirmDirectPaymentTransaction
     */
    public function setTransactionId($transactionId = null)
    {
        // validation for constraint: string
        if (!is_null($transactionId) && !is_string($transactionId)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($transactionId)), __LINE__);
        }
        if (is_null($transactionId) || (is_array($transactionId) && empty($transactionId))) {
            unset($this->transactionId);
        } else {
            $this->transactionId = $transactionId;
        }
        return $this;
    }
    /**
     * Get pinCode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPinCode()
    {
        return isset($this->pinCode) ? $this->pinCode : null;
    }
    /**
     * Set pinCode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $pinCode
     * @return \Cashu\TPay\DirectBilling\Structs\ConfirmDirectPaymentTransaction
     */
    public function setPinCode($pinCode = null)
    {
        // validation for constraint: string
        if (!is_null($pinCode) && !is_string($pinCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($pinCode)), __LINE__);
        }
        if (is_null($pinCode) || (is_array($pinCode) && empty($pinCode))) {
            unset($this->pinCode);
        } else {
            $this->pinCode = $pinCode;
        }
        return $this;
    }
    /**
     * Get signature value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSignature()
    {
        return isset($this->signature) ? $this->signature : null;
    }
    /**
     * Set signature value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $signature
     * @return \Cashu\TPay\DirectBilling\Structs\ConfirmDirectPaymentTransaction
     */
    public function setSignature($signature = null)
    {
        // validation for constraint: string
        if (!is_null($signature) && !is_string($signature)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($signature)), __LINE__);
        }
        if (is_null($signature) || (is_array($signature) && empty($signature))) {
            unset($this->signature);
        } else {
            $this->signature = $signature;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \Cashu\TPay\DirectBilling\Structs\ConfirmDirectPaymentTransaction
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}

<?php

namespace Cashu\TPay\DirectBilling\Structs;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ResendVerificationPinResponse Structs
 * @subpackage Structs
 */
class ResendVerificationPinResponse extends AbstractStructBase
{
    /**
     * The ResendVerificationPinResult
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \Cashu\TPay\DirectBilling\Structs\TpayResponse
     */
    public $ResendVerificationPinResult;
    /**
     * Constructor method for ResendVerificationPinResponse
     * @uses ResendVerificationPinResponse::setResendVerificationPinResult()
     * @param \Cashu\TPay\DirectBilling\Structs\TpayResponse $resendVerificationPinResult
     */
    public function __construct(\Cashu\TPay\DirectBilling\Structs\TpayResponse $resendVerificationPinResult = null)
    {
        $this
            ->setResendVerificationPinResult($resendVerificationPinResult);
    }
    /**
     * Get ResendVerificationPinResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \Cashu\TPay\DirectBilling\Structs\TpayResponse|null
     */
    public function getResendVerificationPinResult()
    {
        return isset($this->ResendVerificationPinResult) ? $this->ResendVerificationPinResult : null;
    }
    /**
     * Set ResendVerificationPinResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \Cashu\TPay\DirectBilling\Structs\TpayResponse $resendVerificationPinResult
     * @return \Cashu\TPay\DirectBilling\Structs\ResendVerificationPinResponse
     */
    public function setResendVerificationPinResult(\Cashu\TPay\DirectBilling\Structs\TpayResponse $resendVerificationPinResult = null)
    {
        if (is_null($resendVerificationPinResult) || (is_array($resendVerificationPinResult) && empty($resendVerificationPinResult))) {
            unset($this->ResendVerificationPinResult);
        } else {
            $this->ResendVerificationPinResult = $resendVerificationPinResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \Cashu\TPay\DirectBilling\Structs\ResendVerificationPinResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}

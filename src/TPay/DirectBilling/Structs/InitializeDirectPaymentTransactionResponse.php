<?php

namespace Cashu\TPay\DirectBilling\Structs;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for InitializeDirectPaymentTransactionResponse Structs
 * @subpackage Structs
 */
class InitializeDirectPaymentTransactionResponse extends AbstractStructBase
{
    /**
     * The InitializeDirectPaymentTransactionResult
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \Cashu\TPay\DirectBilling\Structs\DirectPaymentResponse
     */
    public $InitializeDirectPaymentTransactionResult;
    /**
     * Constructor method for InitializeDirectPaymentTransactionResponse
     * @uses InitializeDirectPaymentTransactionResponse::setInitializeDirectPaymentTransactionResult()
     * @param \Cashu\TPay\DirectBilling\Structs\DirectPaymentResponse $initializeDirectPaymentTransactionResult
     */
    public function __construct(\Cashu\TPay\DirectBilling\Structs\DirectPaymentResponse $initializeDirectPaymentTransactionResult = null)
    {
        $this
            ->setInitializeDirectPaymentTransactionResult($initializeDirectPaymentTransactionResult);
    }
    /**
     * Get InitializeDirectPaymentTransactionResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \Cashu\TPay\DirectBilling\Structs\DirectPaymentResponse|null
     */
    public function getInitializeDirectPaymentTransactionResult()
    {
        return isset($this->InitializeDirectPaymentTransactionResult) ? $this->InitializeDirectPaymentTransactionResult : null;
    }
    /**
     * Set InitializeDirectPaymentTransactionResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \Cashu\TPay\DirectBilling\Structs\DirectPaymentResponse $initializeDirectPaymentTransactionResult
     * @return \Cashu\TPay\DirectBilling\Structs\InitializeDirectPaymentTransactionResponse
     */
    public function setInitializeDirectPaymentTransactionResult(\Cashu\TPay\DirectBilling\Structs\DirectPaymentResponse $initializeDirectPaymentTransactionResult = null)
    {
        if (is_null($initializeDirectPaymentTransactionResult) || (is_array($initializeDirectPaymentTransactionResult) && empty($initializeDirectPaymentTransactionResult))) {
            unset($this->InitializeDirectPaymentTransactionResult);
        } else {
            $this->InitializeDirectPaymentTransactionResult = $initializeDirectPaymentTransactionResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \Cashu\TPay\DirectBilling\Structs\InitializeDirectPaymentTransactionResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}

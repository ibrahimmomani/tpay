<?php

namespace Cashu\TPay\DirectBilling\Structs;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for directPaymentResponse Structs
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:directPaymentResponse
 * @subpackage Structs
 */
class DirectPaymentResponse extends AbstractStructBase
{
    /**
     * The operationStatusCode
     * @var string
     */
    public $operationStatusCode;
    /**
     * The transactionId
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $transactionId;
    /**
     * The amountCharged
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var float
     */
    public $amountCharged;
    /**
     * The currencyCode
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $currencyCode;
    /**
     * The errorMessage
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $errorMessage;
    /**
     * The details
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $details;
    /**
     * Constructor method for directPaymentResponse
     * @uses DirectPaymentResponse::setOperationStatusCode()
     * @uses DirectPaymentResponse::setTransactionId()
     * @uses DirectPaymentResponse::setAmountCharged()
     * @uses DirectPaymentResponse::setCurrencyCode()
     * @uses DirectPaymentResponse::setErrorMessage()
     * @uses DirectPaymentResponse::setDetails()
     * @param string $operationStatusCode
     * @param string $transactionId
     * @param float $amountCharged
     * @param string $currencyCode
     * @param string $errorMessage
     * @param string $details
     */
    public function __construct($operationStatusCode = null, $transactionId = null, $amountCharged = null, $currencyCode = null, $errorMessage = null, $details = null)
    {
        $this
            ->setOperationStatusCode($operationStatusCode)
            ->setTransactionId($transactionId)
            ->setAmountCharged($amountCharged)
            ->setCurrencyCode($currencyCode)
            ->setErrorMessage($errorMessage)
            ->setDetails($details);
    }
    /**
     * Get operationStatusCode value
     * @return string|null
     */
    public function getOperationStatusCode()
    {
        return $this->operationStatusCode;
    }
    /**
     * Set operationStatusCode value
     * @uses \Cashu\TPay\DirectBilling\Enums\DirectPaymentStatus::valueIsValid()
     * @uses \Cashu\TPay\DirectBilling\Enums\DirectPaymentStatus::getValidValues()
     * @throws \InvalidArgumentException
     * @param string $operationStatusCode
     * @return \Cashu\TPay\DirectBilling\Structs\DirectPaymentResponse
     */
    public function setOperationStatusCode($operationStatusCode = null)
    {
        // validation for constraint: enumeration
        if (!\Cashu\TPay\DirectBilling\Enums\DirectPaymentStatus::valueIsValid($operationStatusCode)) {
            throw new \InvalidArgumentException(sprintf('Value "%s" is invalid, please use one of: %s', $operationStatusCode, implode(', ', \Cashu\TPay\DirectBilling\Enums\DirectPaymentStatus::getValidValues())), __LINE__);
        }
        $this->operationStatusCode = $operationStatusCode;
        return $this;
    }
    /**
     * Get transactionId value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getTransactionId()
    {
        return isset($this->transactionId) ? $this->transactionId : null;
    }
    /**
     * Set transactionId value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $transactionId
     * @return \Cashu\TPay\DirectBilling\Structs\DirectPaymentResponse
     */
    public function setTransactionId($transactionId = null)
    {
        // validation for constraint: string
        if (!is_null($transactionId) && !is_string($transactionId)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($transactionId)), __LINE__);
        }
        if (is_null($transactionId) || (is_array($transactionId) && empty($transactionId))) {
            unset($this->transactionId);
        } else {
            $this->transactionId = $transactionId;
        }
        return $this;
    }
    /**
     * Get amountCharged value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return float|null
     */
    public function getAmountCharged()
    {
        return isset($this->amountCharged) ? $this->amountCharged : null;
    }
    /**
     * Set amountCharged value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param float $amountCharged
     * @return \Cashu\TPay\DirectBilling\Structs\DirectPaymentResponse
     */
    public function setAmountCharged($amountCharged = null)
    {
        if (is_null($amountCharged) || (is_array($amountCharged) && empty($amountCharged))) {
            unset($this->amountCharged);
        } else {
            $this->amountCharged = $amountCharged;
        }
        return $this;
    }
    /**
     * Get currencyCode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCurrencyCode()
    {
        return isset($this->currencyCode) ? $this->currencyCode : null;
    }
    /**
     * Set currencyCode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $currencyCode
     * @return \Cashu\TPay\DirectBilling\Structs\DirectPaymentResponse
     */
    public function setCurrencyCode($currencyCode = null)
    {
        // validation for constraint: string
        if (!is_null($currencyCode) && !is_string($currencyCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($currencyCode)), __LINE__);
        }
        if (is_null($currencyCode) || (is_array($currencyCode) && empty($currencyCode))) {
            unset($this->currencyCode);
        } else {
            $this->currencyCode = $currencyCode;
        }
        return $this;
    }
    /**
     * Get errorMessage value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getErrorMessage()
    {
        return isset($this->errorMessage) ? $this->errorMessage : null;
    }
    /**
     * Set errorMessage value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $errorMessage
     * @return \Cashu\TPay\DirectBilling\Structs\DirectPaymentResponse
     */
    public function setErrorMessage($errorMessage = null)
    {
        // validation for constraint: string
        if (!is_null($errorMessage) && !is_string($errorMessage)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($errorMessage)), __LINE__);
        }
        if (is_null($errorMessage) || (is_array($errorMessage) && empty($errorMessage))) {
            unset($this->errorMessage);
        } else {
            $this->errorMessage = $errorMessage;
        }
        return $this;
    }
    /**
     * Get details value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getDetails()
    {
        return isset($this->details) ? $this->details : null;
    }
    /**
     * Set details value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $details
     * @return \Cashu\TPay\DirectBilling\Structs\DirectPaymentResponse
     */
    public function setDetails($details = null)
    {
        // validation for constraint: string
        if (!is_null($details) && !is_string($details)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($details)), __LINE__);
        }
        if (is_null($details) || (is_array($details) && empty($details))) {
            unset($this->details);
        } else {
            $this->details = $details;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \Cashu\TPay\DirectBilling\Structs\DirectPaymentResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}

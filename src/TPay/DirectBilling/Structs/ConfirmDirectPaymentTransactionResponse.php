<?php

namespace Cashu\TPay\DirectBilling\Structs;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ConfirmDirectPaymentTransactionResponse Structs
 * @subpackage Structs
 */
class ConfirmDirectPaymentTransactionResponse extends AbstractStructBase
{
    /**
     * The ConfirmDirectPaymentTransactionResult
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \Cashu\TPay\DirectBilling\Structs\DirectPaymentResponse
     */
    public $ConfirmDirectPaymentTransactionResult;
    /**
     * Constructor method for ConfirmDirectPaymentTransactionResponse
     * @uses ConfirmDirectPaymentTransactionResponse::setConfirmDirectPaymentTransactionResult()
     * @param \Cashu\TPay\DirectBilling\Structs\DirectPaymentResponse $confirmDirectPaymentTransactionResult
     */
    public function __construct(\Cashu\TPay\DirectBilling\Structs\DirectPaymentResponse $confirmDirectPaymentTransactionResult = null)
    {
        $this
            ->setConfirmDirectPaymentTransactionResult($confirmDirectPaymentTransactionResult);
    }
    /**
     * Get ConfirmDirectPaymentTransactionResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \Cashu\TPay\DirectBilling\Structs\DirectPaymentResponse|null
     */
    public function getConfirmDirectPaymentTransactionResult()
    {
        return isset($this->ConfirmDirectPaymentTransactionResult) ? $this->ConfirmDirectPaymentTransactionResult : null;
    }
    /**
     * Set ConfirmDirectPaymentTransactionResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \Cashu\TPay\DirectBilling\Structs\DirectPaymentResponse $confirmDirectPaymentTransactionResult
     * @return \Cashu\TPay\DirectBilling\Structs\ConfirmDirectPaymentTransactionResponse
     */
    public function setConfirmDirectPaymentTransactionResult(\Cashu\TPay\DirectBilling\Structs\DirectPaymentResponse $confirmDirectPaymentTransactionResult = null)
    {
        if (is_null($confirmDirectPaymentTransactionResult) || (is_array($confirmDirectPaymentTransactionResult) && empty($confirmDirectPaymentTransactionResult))) {
            unset($this->ConfirmDirectPaymentTransactionResult);
        } else {
            $this->ConfirmDirectPaymentTransactionResult = $confirmDirectPaymentTransactionResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \Cashu\TPay\DirectBilling\Structs\ConfirmDirectPaymentTransactionResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}

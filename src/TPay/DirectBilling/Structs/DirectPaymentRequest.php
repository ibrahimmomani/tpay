<?php

namespace Cashu\TPay\DirectBilling\Structs;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for directPaymentRequest Structs
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:directPaymentRequest
 * @subpackage Structs
 */
class DirectPaymentRequest extends AbstractStructBase
{
    /**
     * The signature
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $signature;
    /**
     * The productCatalogName
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $productCatalogName;
    /**
     * The productId
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $productId;
    /**
     * The msisdn
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $msisdn;
    /**
     * The operatorCode
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $operatorCode;
    /**
     * The orderInfo
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $orderInfo;
    /**
     * The language
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $language;
    /**
     * Constructor method for directPaymentRequest
     * @uses DirectPaymentRequest::setSignature()
     * @uses DirectPaymentRequest::setProductCatalogName()
     * @uses DirectPaymentRequest::setProductId()
     * @uses DirectPaymentRequest::setMsisdn()
     * @uses DirectPaymentRequest::setOperatorCode()
     * @uses DirectPaymentRequest::setOrderInfo()
     * @uses DirectPaymentRequest::setLanguage()
     * @param string $signature
     * @param string $productCatalogName
     * @param string $productId
     * @param string $msisdn
     * @param string $operatorCode
     * @param string $orderInfo
     * @param string $language
     */
    public function __construct($signature = null, $productCatalogName = null, $productId = null, $msisdn = null, $operatorCode = null, $orderInfo = null, $language = null)
    {
        $this
            ->setSignature($signature)
            ->setProductCatalogName($productCatalogName)
            ->setProductId($productId)
            ->setMsisdn($msisdn)
            ->setOperatorCode($operatorCode)
            ->setOrderInfo($orderInfo)
            ->setLanguage($language);
    }
    /**
     * Get signature value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSignature()
    {
        return isset($this->signature) ? $this->signature : null;
    }
    /**
     * Set signature value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $signature
     * @return \Cashu\TPay\DirectBilling\Structs\DirectPaymentRequest
     */
    public function setSignature($signature = null)
    {
        // validation for constraint: string
        if (!is_null($signature) && !is_string($signature)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($signature)), __LINE__);
        }
        if (is_null($signature) || (is_array($signature) && empty($signature))) {
            unset($this->signature);
        } else {
            $this->signature = $signature;
        }
        return $this;
    }
    /**
     * Get productCatalogName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getProductCatalogName()
    {
        return isset($this->productCatalogName) ? $this->productCatalogName : null;
    }
    /**
     * Set productCatalogName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $productCatalogName
     * @return \Cashu\TPay\DirectBilling\Structs\DirectPaymentRequest
     */
    public function setProductCatalogName($productCatalogName = null)
    {
        // validation for constraint: string
        if (!is_null($productCatalogName) && !is_string($productCatalogName)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($productCatalogName)), __LINE__);
        }
        if (is_null($productCatalogName) || (is_array($productCatalogName) && empty($productCatalogName))) {
            unset($this->productCatalogName);
        } else {
            $this->productCatalogName = $productCatalogName;
        }
        return $this;
    }
    /**
     * Get productId value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getProductId()
    {
        return isset($this->productId) ? $this->productId : null;
    }
    /**
     * Set productId value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $productId
     * @return \Cashu\TPay\DirectBilling\Structs\DirectPaymentRequest
     */
    public function setProductId($productId = null)
    {
        // validation for constraint: string
        if (!is_null($productId) && !is_string($productId)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($productId)), __LINE__);
        }
        if (is_null($productId) || (is_array($productId) && empty($productId))) {
            unset($this->productId);
        } else {
            $this->productId = $productId;
        }
        return $this;
    }
    /**
     * Get msisdn value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getMsisdn()
    {
        return isset($this->msisdn) ? $this->msisdn : null;
    }
    /**
     * Set msisdn value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $msisdn
     * @return \Cashu\TPay\DirectBilling\Structs\DirectPaymentRequest
     */
    public function setMsisdn($msisdn = null)
    {
        // validation for constraint: string
        if (!is_null($msisdn) && !is_string($msisdn)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($msisdn)), __LINE__);
        }
        if (is_null($msisdn) || (is_array($msisdn) && empty($msisdn))) {
            unset($this->msisdn);
        } else {
            $this->msisdn = $msisdn;
        }
        return $this;
    }
    /**
     * Get operatorCode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getOperatorCode()
    {
        return isset($this->operatorCode) ? $this->operatorCode : null;
    }
    /**
     * Set operatorCode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $operatorCode
     * @return \Cashu\TPay\DirectBilling\Structs\DirectPaymentRequest
     */
    public function setOperatorCode($operatorCode = null)
    {
        // validation for constraint: string
        if (!is_null($operatorCode) && !is_string($operatorCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($operatorCode)), __LINE__);
        }
        if (is_null($operatorCode) || (is_array($operatorCode) && empty($operatorCode))) {
            unset($this->operatorCode);
        } else {
            $this->operatorCode = $operatorCode;
        }
        return $this;
    }
    /**
     * Get orderInfo value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getOrderInfo()
    {
        return isset($this->orderInfo) ? $this->orderInfo : null;
    }
    /**
     * Set orderInfo value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $orderInfo
     * @return \Cashu\TPay\DirectBilling\Structs\DirectPaymentRequest
     */
    public function setOrderInfo($orderInfo = null)
    {
        // validation for constraint: string
        if (!is_null($orderInfo) && !is_string($orderInfo)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($orderInfo)), __LINE__);
        }
        if (is_null($orderInfo) || (is_array($orderInfo) && empty($orderInfo))) {
            unset($this->orderInfo);
        } else {
            $this->orderInfo = $orderInfo;
        }
        return $this;
    }
    /**
     * Get language value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getLanguage()
    {
        return isset($this->language) ? $this->language : null;
    }
    /**
     * Set language value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @uses \Cashu\TPay\DirectBilling\Enums\MerchantLanguage::valueIsValid()
     * @uses \Cashu\TPay\DirectBilling\Enums\MerchantLanguage::getValidValues()
     * @throws \InvalidArgumentException
     * @param string $language
     * @return \Cashu\TPay\DirectBilling\Structs\DirectPaymentRequest
     */
    public function setLanguage($language = null)
    {
        // validation for constraint: enumeration
        if (!\Cashu\TPay\DirectBilling\Enums\MerchantLanguage::valueIsValid($language)) {
            throw new \InvalidArgumentException(sprintf('Value "%s" is invalid, please use one of: %s', $language, implode(', ', \Cashu\TPay\DirectBilling\Enums\MerchantLanguage::getValidValues())), __LINE__);
        }
        if (is_null($language) || (is_array($language) && empty($language))) {
            unset($this->language);
        } else {
            $this->language = $language;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \Cashu\TPay\DirectBilling\Structs\DirectPaymentRequest
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}

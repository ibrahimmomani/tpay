<?php

namespace Cashu\TPay\DirectBilling\Structs;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ResendVerificationPin Structs
 * @subpackage Structs
 */
class ResendVerificationPin extends AbstractStructBase
{
    /**
     * The transactionId
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $transactionId;
    /**
     * The language
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $language;
    /**
     * The signature
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $signature;
    /**
     * Constructor method for ResendVerificationPin
     * @uses ResendVerificationPin::setTransactionId()
     * @uses ResendVerificationPin::setLanguage()
     * @uses ResendVerificationPin::setSignature()
     * @param string $transactionId
     * @param string $language
     * @param string $signature
     */
    public function __construct($transactionId = null, $language = null, $signature = null)
    {
        $this
            ->setTransactionId($transactionId)
            ->setLanguage($language)
            ->setSignature($signature);
    }
    /**
     * Get transactionId value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getTransactionId()
    {
        return isset($this->transactionId) ? $this->transactionId : null;
    }
    /**
     * Set transactionId value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $transactionId
     * @return \Cashu\TPay\DirectBilling\Structs\ResendVerificationPin
     */
    public function setTransactionId($transactionId = null)
    {
        // validation for constraint: string
        if (!is_null($transactionId) && !is_string($transactionId)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($transactionId)), __LINE__);
        }
        if (is_null($transactionId) || (is_array($transactionId) && empty($transactionId))) {
            unset($this->transactionId);
        } else {
            $this->transactionId = $transactionId;
        }
        return $this;
    }
    /**
     * Get language value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getLanguage()
    {
        return isset($this->language) ? $this->language : null;
    }
    /**
     * Set language value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @uses \Cashu\TPay\DirectBilling\Enums\MerchantLanguage::valueIsValid()
     * @uses \Cashu\TPay\DirectBilling\Enums\MerchantLanguage::getValidValues()
     * @throws \InvalidArgumentException
     * @param string $language
     * @return \Cashu\TPay\DirectBilling\Structs\ResendVerificationPin
     */
    public function setLanguage($language = null)
    {
        // validation for constraint: enumeration
        if (!\Cashu\TPay\DirectBilling\Enums\MerchantLanguage::valueIsValid($language)) {
            throw new \InvalidArgumentException(sprintf('Value "%s" is invalid, please use one of: %s', $language, implode(', ', \Cashu\TPay\DirectBilling\Enums\MerchantLanguage::getValidValues())), __LINE__);
        }
        if (is_null($language) || (is_array($language) && empty($language))) {
            unset($this->language);
        } else {
            $this->language = $language;
        }
        return $this;
    }
    /**
     * Get signature value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSignature()
    {
        return isset($this->signature) ? $this->signature : null;
    }
    /**
     * Set signature value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $signature
     * @return \Cashu\TPay\DirectBilling\Structs\ResendVerificationPin
     */
    public function setSignature($signature = null)
    {
        // validation for constraint: string
        if (!is_null($signature) && !is_string($signature)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($signature)), __LINE__);
        }
        if (is_null($signature) || (is_array($signature) && empty($signature))) {
            unset($this->signature);
        } else {
            $this->signature = $signature;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \Cashu\TPay\DirectBilling\Structs\ResendVerificationPin
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}

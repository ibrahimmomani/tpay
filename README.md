#CASHU TPAY

Add This snippet to your "composer.json" 
```json
{
    "require": {
    	"php": ">=5.3.3",
        "ext-soap": "*",
        "wsdltophp/packagebase": "dev-master",
        "cashu/tpay": "dev-master"
    },
    "repositories": [
        {
            "type": "vcs",
            "url":  "git@bitbucket.org:ibrahimmomani/tpay.git"
        }
    ]
}
```

Then run 
```sh
composer install
```
### Examples how to use

This below using the catalogs endpoint.

```php
require_once __DIR__ . '/vendor/autoload.php';
/**
 * Minimal options
 */
$options = array(
    \WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_URL => 'http://staging.tpay.me/api/TPayCatalogs.svc?wsdl',
    \WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_CLASSMAP => \Cashu\TPay\Catalogs\ClassMap::get(),
);
/**
 * Samples for Get ServiceType
 */
$get = new \Cashu\TPay\Catalogs\Services\Get($options);
/**
 * Sample call for GetMobileOperators operation/method
 */
if ($get->GetMobileOperators(new \Cashu\TPay\Catalogs\Structs\GetMobileOperators()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
```

This one is using DirectBilling soap endpoint

```php
require_once __DIR__ . '/vendor/autoload.php';
/**
 * Minimal options
 */
$options = array(
    \WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_URL => 'http://staging.tpay.me/api/TPay.svc?wsdl',
    \WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_CLASSMAP => \Cashu\TPay\DirectBilling\ClassMap::get(),
);
/**
 * Samples for Initialize ServiceType
 */
$initializeService = new \Cashu\TPay\DirectBilling\Services\DirectBillingInitializeService($options);
/**
 * Sample call for InitializeDirectPaymentTransaction operation/method
 */

if ($initializeService
        ->InitializeDirectPaymentTransaction(
            new \Cashu\TPay\DirectBilling\Structs\InitializeDirectPaymentTransaction(
                new \Cashu\TPay\DirectBilling\Structs\DirectPaymentRequest()
            )) !== false) {
    print_r($initializeService->getResult());
} else {
    print_r($initializeService->getLastError());
}
```

